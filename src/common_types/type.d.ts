import { DocumentUser } from '../db/models/User';
import { Nullable } from './TypeUtils';

declare global {

  declare namespace Express {
    /** Extends express.Request */
    interface Request {
      user: Nullable<DocumentUser>;
    }
  }
}