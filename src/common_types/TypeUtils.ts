import { Document } from 'mongoose';

/** nullable type alias */
export type Nullable<T> = T | null;

/** Модель */
export type DocumentModel<MODEL> = Document<MODEL> & MODEL;