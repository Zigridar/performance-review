import { ReviewType } from './interfaces/Review';
import { IUser } from './interfaces/User';

export interface AuthBody {
  email: string;
  password: string;
}

const RESPONSE_ERROR = 'Error';
const RESPONSE_RESULT = 'Result';

type ResponseErrorType = {
  type: typeof RESPONSE_ERROR;
  message: string;
};

type ResponseResultType<DATA> = {
  type: typeof RESPONSE_RESULT;
  data?: DATA;
};

/** Ответ на запрос элементов */
export type GetResponse<MODEL> = {
  items: Array<MODEL>;
  count?: number;
};

/** Фильтрация по тексту + пагинация */
export type QueryFilter<FILTERS = object> = {
  id?: string;
  term?: string;
  currentPage?: number;
  pageSize?: number;
  additionalFilters?: FILTERS;
};

export type ResType<DATA> = ResponseResultType<DATA> | ResponseErrorType;

/** Сущность с id */
export interface IWithId {
  id: string;
}

export type FormAdditionFilters = {
  formType?: ReviewType;
};

export type PRAdditionalFilter = {
  actual?: boolean;
};

export type AroundDescription = IWithId & {
  user: IUser;
  begin: number;
  end: number;
};
