import { Nullable } from '../TypeUtils';
import { INamed } from './INamed';

export const HR = 'HR';
export const ADMIN = 'ADMIN';
export const LEADER = 'LEADER';
export const EMPLOYEE = 'EMPLOYEE';

/** Роли пользователей */
export const userRoles: () => Array<UserRole> = () => ([
  HR,
  ADMIN,
  LEADER,
  EMPLOYEE,
]);

/** Роль пользователя */
export type UserRole = typeof HR | typeof ADMIN | typeof LEADER | typeof EMPLOYEE;

export interface IUser {
  id: string;
  name: string;
  lastName: string;
  middleName?: string;
  email: string;
  phone: string;
  photo?: string;
  post: Nullable<INamed>;
  project?: Nullable<INamed>;
  team?: Nullable<INamed>;
  leader: Nullable<IUser>;
  hr: Nullable<IUser>;
  hashedPassword?: string;
  roles: Array<UserRole>;
}
