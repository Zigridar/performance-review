import { IAnswer, IQuestion } from './Question';
import { ReviewType } from './Review';
import { IUser } from './User';

export interface IRate {
  scoreFrom: number;
  scoreTo: number;
  mark: string;
}

export interface IQuestionInfo {
  question: IQuestion;
  rate: number;
}

/** Шаблон теста */
export interface IForm {
  id: string;
  description: string;
  type: ReviewType;
  questions: Array<IQuestionInfo>;
  graded: boolean;
  rating: Array<IRate>;
  modifyDate: number;
  author: string;
  users?: Array<IUser>;
}

//todo
interface FormResult {
  form: IForm;
  userAnswers: Array<Array<IAnswer>>;
}