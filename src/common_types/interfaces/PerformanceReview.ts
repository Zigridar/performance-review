import { IForm } from './Form';
import { IUser } from './User';

export interface IPerformanceReview {
  id: string;
  author: IUser;
  employee: IUser;
  period: [number, number];
  forms: Array<IForm>;
}