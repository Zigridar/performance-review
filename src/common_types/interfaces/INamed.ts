import { IWithId } from '../API_types';

export interface INamed extends IWithId {
  name: string;
}