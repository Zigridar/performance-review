/** Ответ */
import { ReviewType } from './Review';

export interface IAnswer {
  id: string;
  text: string;
  isCorrect: boolean;
}

/** Вопрос */
export interface IQuestion {
  id: string;
  text: string;
  type: QuestionType;
  reviewTypes: Array<ReviewType>;
  answers: Array<IAnswer>;
}


/** Типы вопросов */
export const CLOSED = 'CLOSED';
export const OPEN = 'OPEN';

export type QuestionType = typeof CLOSED | typeof OPEN;

/** Все типы вопросов */
export const questionTypes: () => Array<QuestionType> = () => ([
  CLOSED,
  OPEN,
]);

