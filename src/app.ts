import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import express, { Express, Router } from 'express';
import http from 'http';
import path from 'path';
import { APIPath } from './APIPath';
import { getConfig } from './config';
import { dbConnect } from './db/connect';
import { createUserIfNotExists } from './db/models/User';
import { aroundRouter } from './routes/around.route';
import { authRouter } from './routes/auth.route';
import { formRouter } from './routes/form.route';
import { performanceReviewRouter } from './routes/performance-review.route';
import { postRouter } from './routes/post.route';
import { projectRouter } from './routes/project..route';
import { questionRouter } from './routes/question.route';
import { teamRouter } from './routes/team.route';
import { userRouter } from './routes/user.route';

/** app config */
const config = getConfig();

export const IS_DEBUG = config.MODE !== 'production';

/** init server application **/
const app: Express = express();

/** create http server for application requests */
const server = http.createServer(app);

/** set body parser **/
app.use(bodyParser.json());

/** set cookie parser */
app.use(cookieParser());

/** API router */
const apiRouter = Router();

/** init API router */
app.use('/api', apiRouter);

/** run server */
server.listen(config.PORT, () => {
  console.info(`Server startup ${new Date().toUTCString()} on port: ${config.PORT}`);
});

/** set auth router */
apiRouter.use(APIPath.auth, authRouter(config.JWT_SECRET));

/** set user router */
apiRouter.use(APIPath.users, userRouter(config.JWT_SECRET));

/** set question router */
apiRouter.use(APIPath.question, questionRouter(config.JWT_SECRET));

/** set form router */
apiRouter.use(APIPath.form, formRouter(config.JWT_SECRET));

/** set performance review router */
apiRouter.use(APIPath.pr, performanceReviewRouter(config.JWT_SECRET));

/** set post router */
apiRouter.use(APIPath.post, postRouter(config.JWT_SECRET));

/** set team router */
apiRouter.use(APIPath.team, teamRouter(config.JWT_SECRET));

/** set project router */
apiRouter.use(APIPath.project, projectRouter(config.JWT_SECRET));

/** set around router */
apiRouter.use(APIPath.around, aroundRouter(config.JWT_SECRET));

/** Production mode **/
if (!IS_DEBUG) {
  app.use('/', express.static(path.join(__dirname, 'front')));

  app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, 'front/index.html'));
  });
}

/** init db connection */
dbConnect(config.MONGO_URI, config.MONGO_USER,  config.MONGO_PASS)
  .then(() => {
    createUserIfNotExists()
      .then(user => {
        if (user)
          console.info(`User with email ${user.email} was successfully created`);
      });
  })
  .catch((err: Error) => {
    console.error('DB CONNECTION ERROR');
    console.error(err.message);
    process.exit(1);
  });
