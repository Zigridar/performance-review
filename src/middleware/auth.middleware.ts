import express from 'express';
import { StatusCodes } from 'http-status-codes';
import jwt, { JwtPayload } from 'jsonwebtoken';
import { ResType } from '../common_types/API_types';
import { populateUser, UserModel } from '../db/models/User';

/**
 * Use the middleware for any routes (except auth.routes.ts)
 * */
const auth = (jwtSecret: string) => async (req: express.Request, res: express.Response<ResType<void>>, next: () => void) => {

  /** break if test request **/
  if (req.method === 'OPTIONS')
    return next();

  try {
    /** parse token from authorization header **/
    const token = req?.cookies?.token;

    /** break if token non-valid **/
    if (!token)
      return res
        .clearCookie('token')
        .status(StatusCodes.UNAUTHORIZED)
        .json({
          type: 'Error',
          message: 'no authorization',
        });

    const email: string = (jwt.verify(token, jwtSecret) as JwtPayload).email;

    /** set user jwt object  to request **/
    req.user = await populateUser(UserModel.findOne({ email }));
    /** apply next handler **/
    next();
  } catch (e) {
    res
      .clearCookie('token')
      .status(StatusCodes.UNAUTHORIZED)
      .json({
        type: 'Error',
        message: 'no authorization',
      });
  }
};

export default auth;