import { INamed } from '../common_types/interfaces/INamed';
import { TeamModel } from '../db/models/Team';
import { createCRUDRouter, likeExpression } from './createRouter';

export const teamRouter = (jwtSecret: string) => {
  return createCRUDRouter<INamed>(jwtSecret, TeamModel, {
    createFilter: ({ term }) => {
      return term ? { name: likeExpression(term) } : {};
    },
  });
};