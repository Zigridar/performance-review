import { FilterQuery } from 'mongoose';
import { FormAdditionFilters } from '../common_types/API_types';
import { IForm } from '../common_types/interfaces/Form';
import { DocumentModel } from '../common_types/TypeUtils';
import { FormModel } from '../db/models/Form';
import { createCRUDRouter, likeExpression } from './createRouter';

export const formRouter = (jwtSecret: string) =>
  createCRUDRouter<IForm, FormAdditionFilters>(jwtSecret, FormModel, {
    createFilter: (filter) => {
      const { term, additionalFilters } = filter;
      const res: FilterQuery<DocumentModel<IForm>> = {};
      if (term)
        res.description = likeExpression(term);

      if (additionalFilters && additionalFilters.formType){
        res.type = additionalFilters.formType;
      }
      return res;
    },
  });