import { FilterQuery } from 'mongoose';
import { PRAdditionalFilter } from '../common_types/API_types';
import { IPerformanceReview } from '../common_types/interfaces/PerformanceReview';
import { IUser } from '../common_types/interfaces/User';
import { DocumentModel } from '../common_types/TypeUtils';
import { PerformanceReviewModel } from '../db/models/PerformanceReview';
import { isUserEmployee, UserModel } from '../db/models/User';
import { createCRUDRouter } from './createRouter';

export const performanceReviewRouter = (jwtSecret: string) =>
  createCRUDRouter<IPerformanceReview, PRAdditionalFilter>(jwtSecret, PerformanceReviewModel, {
    preSave: async (pr, data) => {
      pr.author = await UserModel.findById(data.author.id) as IUser;
      pr.employee = await UserModel.findById(data.employee.id) as IUser;
    },
    modifyQuery: query => {
      return query
        .populate('author')
        .populate('employee');
    },
    createFilter: (filter, req) => {

      const { additionalFilters } = filter;

      const actual = !!(additionalFilters && additionalFilters.actual);

      const user = req.user!;

      /** Простой сотрудник */
      const isEmployee = isUserEmployee(user);

      const res: FilterQuery<DocumentModel<IPerformanceReview>> = {};

      /** На сотрудника */
      if (isEmployee) {
        res.employee = user;
      }

      //todo
      // if (actual) {
      //   const now = Date.now();
      //
      //   // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      //   //@ts-ignore
      //   res.period = {
      //     '0': { $gte: now },
      //   };
      //
      //   res['period.1'] = {
      //     $le: now,
      //   };
      // }

      return res;
    },
    mapBody: pr => {
      pr.forms.forEach(form => {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        form.users = form?.users?.map(user => user.id);
      });
      return pr;
    },
  });