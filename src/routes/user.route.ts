import { ProjectModel } from '../db/models/Poject';
import { PostModel } from '../db/models/Post';
import { TeamModel } from '../db/models/Team';
import { hashPassword, populateUser, UserModel } from '../db/models/User';
import { createCRUDRouter, likeExpression } from './createRouter';

export const userRouter = (jwtSecret: string) =>
  createCRUDRouter(jwtSecret, UserModel, {
    excludedKeys: ['hr', 'post', 'team', 'leader', 'project'],
    preSave: async (user, data, exist) => {
      if (!exist)
        user.hashedPassword = await hashPassword(user.hashedPassword);

      if (data.team) {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        user.team = await TeamModel.findById(data.team.id);
      } else {
        user.team = null;
      }

      if (data.post) {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        user.post = await PostModel.findById(data.post.id);
      } else {
        user.post = null;
      }

      if (data.leader) {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        user.leader = await UserModel.findById(data.leader.id);
      } else {
        user.leader = null;
      }

      if (data.project) {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        user.project = await ProjectModel.findById(data.project.id);
      } else {
        user.project = null;
      }

      if (data.hr) {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        user.hr = await UserModel.findById(data.hr.id);
      } else {
        user.hr = null;
      }
    },
    createFilter: (filter) => {
      const { term } = filter;
      if (term) {
        return {
          $or: [
            {
              lastName: likeExpression(term),
            },
            {
              name: likeExpression(term),
            },
            {
              middleName: likeExpression(term),
            },
            {
              email: likeExpression(term),
            },
            {
              email: likeExpression(term),
            },
          ],
        };
      } else return {};
    },
    modifyQuery: populateUser,
    mapResult: (user) => {
      delete user.hashedPassword;
      return user;
    },
  });