import { IQuestion } from '../common_types/interfaces/Question';
import { QuestionModel } from '../db/models/Question';
import { createCRUDRouter, likeExpression } from './createRouter';

export const questionRouter = (jwtSecret: string) =>
  createCRUDRouter<IQuestion>(jwtSecret, QuestionModel, {
    createFilter: (filter) => {
      const { term } = filter;
      if (term) {
        return {
          text: likeExpression(term),
        };
      } else return {};
    },
  });
