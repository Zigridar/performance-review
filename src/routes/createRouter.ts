import express, { Router } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { StatusCodes } from 'http-status-codes';
import { FilterQuery, Model, Query } from 'mongoose';
import { GetResponse, IWithId, QueryFilter, ResType } from '../common_types/API_types';
import { DocumentModel, Nullable } from '../common_types/TypeUtils';
import auth from '../middleware/auth.middleware';

const parseFilters: <FILTERS>(data: any) => FILTERS | undefined = <FILTERS>(data: any) => {
  try {
    return JSON.parse(data) as FILTERS;
  } catch (e) {
    return undefined;
  }
};

type Options<MODEL, FILTERS = object> = {
  preSave?: (model: DocumentModel<MODEL>, data: MODEL, exist: boolean) => Promise<void>;
  preDelete?: (model: DocumentModel<MODEL>) => Promise<void>;
  createFilter?: (filter: QueryFilter<FILTERS>, req: express.Request<ParamsDictionary, never, never, QueryFilter<FILTERS>>) => FilterQuery<DocumentModel<MODEL>>;
  modifyQuery?: <T, M>(query: Query<T, M>) => Query<T, M>;
  excludedKeys?: Array<keyof MODEL>;
  mapResult?: (item: MODEL) => MODEL;
  mapBody?: (item: MODEL) => MODEL;
};

/**
 * Создание обобщенного роутера для операций CRUD по модели
 * */
export const createCRUDRouter = <MODEL extends IWithId, FILTERS = object>(
  jwtSecret: string,
  Model: Model<DocumentModel<MODEL>>,
  options: Options<MODEL, FILTERS> = {},
) => {

  const defaultFilter: FilterQuery<DocumentModel<MODEL>> = {};

  const {
    preSave,
    preDelete,
    createFilter = () => defaultFilter,
    modifyQuery = (q: Query<any, any>) => q,
    excludedKeys = [],
    mapResult = (item: MODEL) => item,
    mapBody = (item: MODEL) => item,
  } = options;

  const router = Router();

  router.use(auth(jwtSecret));

  /** Создание */
  router.post('/', async (req: express.Request<ParamsDictionary, never, MODEL>, res: express.Response<ResType<MODEL>>) => {

    try {
      const item: DocumentModel<MODEL> = new Model(mapBody(req.body));

      if (preSave)
        await preSave(item, req.body, false);

      await item.save();

      const data: MODEL = mapResult(item.toJSON() as MODEL);

      res.status(StatusCodes.CREATED).json({
        type: 'Result',
        data,
      });
    } catch (e) {
      console.error(e);
      res.status(StatusCodes.BAD_REQUEST).json({
        type: 'Error',
        message: 'Ошибка при сохранении',
      });
    }
  });

  /** Получение */
  router.get('/', async (req: express.Request<ParamsDictionary, never, never, QueryFilter<FILTERS>>, res: express.Response<ResType<GetResponse<MODEL>>>) => {

    const { id, currentPage: currentPageStr, pageSize: pageSizeStr, term, additionalFilters } = req.query;

    const pageSize = pageSizeStr ? Number(pageSizeStr) : 5;
    const currentPage = currentPageStr ? Number(currentPageStr) : 1;

    try {
      if (id && !term) {
        const item: Nullable<DocumentModel<MODEL>> = await Model.findById(id);

        const result: MODEL = item?.toJSON() as never as MODEL;

        return res.json({
          type: 'Result',
          data: {
            items: [mapResult(result)],
          },
        });
      }

      /** Фильтрация */
      const filter: FilterQuery<DocumentModel<MODEL>> = createFilter({ currentPage, pageSize, term, additionalFilters: parseFilters<FILTERS>(additionalFilters) }, req);

      /** Кол-во элементов, удовлетворяющих фильтрации */
      const allItemsCount = await Model.countDocuments(filter);

      /** Элементы, удовлетворяющие фильтрации + пагинация */
      const items: DocumentModel<MODEL>[] = await modifyQuery(Model.find(filter, null, {
        skip: (currentPage - 1) * pageSize,
        limit: pageSize,
      })).exec();

      const data: Array<MODEL> = items.map(i => mapResult(i.toJSON() as MODEL));

      res.json({
        type: 'Result',
        data: {
          items: data,
          count: allItemsCount,
        },
      });
    } catch (e) {
      console.error(e);
      res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
        type: 'Error',
        message: 'Server error',
      });
    }
  });

  /** Редактирование */
  router.put('/', async (req: express.Request<ParamsDictionary, never, MODEL>, res: express.Response<ResType<MODEL>>) => {

    try {
      const item: Nullable<DocumentModel<MODEL>> = await modifyQuery(Model.findById(req.body?.id)).exec();

      if (!item) {
        res.status(StatusCodes.NOT_FOUND).json({
          type: 'Error',
          message: 'Не найден',
        });
      } else {

        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        for (const key of Object.keys(mapBody(req.body)).filter(key => !excludedKeys.includes(key))) {
          const source = req.body as any;
          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          // @ts-ignore
          item[key] = source[key];
        }

        if (preSave)
          await preSave(item, req.body, true);

        await item.save();

        res.json({
          type: 'Result',
          data: mapResult(item.toJSON() as MODEL),
        });
      }

    } catch (e) {
      console.error(e);
      res.status(StatusCodes.BAD_REQUEST).json({
        type: 'Error',
        message: 'Ошибка при сохранении',
      });
    }
  });

  /** Удаление */
  router.delete('/', async (req: express.Request<ParamsDictionary, never, MODEL>, res: express.Response<ResType<void>>) => {
    try {
      const item: Nullable<DocumentModel<MODEL>> = await Model.findById(req.body?.id);

      if (!item) {
        res.status(StatusCodes.NOT_FOUND).json({
          type: 'Error',
          message: 'Не найден',
        });
      } else {
        if (preDelete)
          await preDelete(item);
        await item.delete();
        res.status(StatusCodes.NO_CONTENT).json({
          type: 'Result',
        });
      }

    } catch (e) {
      console.error(e);
      res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
        type: 'Error',
        message: 'Server error',
      });
    }
  });

  return router;
};

/** Соответствие подстроке */
export const likeExpression: (term: string) => { $regex: string } =
  (term: string) => ({
    $regex: `.*${term}.*`,
  });
