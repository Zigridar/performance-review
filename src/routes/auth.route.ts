import bcrypt from 'bcrypt';
import express, { Router } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { StatusCodes } from 'http-status-codes';
import jwt from 'jsonwebtoken';
import { AuthBody, ResType } from '../common_types/API_types';
import { IUser } from '../common_types/interfaces/User';
import { Nullable } from '../common_types/TypeUtils';
import { DocumentUser, populateUser, UserModel } from '../db/models/User';
import auth from '../middleware/auth.middleware';

export const authRouter: (jwtSecret: string) => Router = (jwtSecret: string) => {

  /** create router */
  const router: Router = Router();

  /** login */
  router.post(
    '/',
    async (req: express.Request<ParamsDictionary, never, AuthBody>, res: express.Response<ResType<IUser>>) => {

      try {

        /** extract "email" and "password" data from request body */
        const { email, password } = req.body;

        /** if it is empty continue login */
        if (!email || !password) {
          return res.status(StatusCodes.BAD_REQUEST).json({
            type: 'Error',
            message: 'Wrong data',
          });
        }

        /** find user by email */
        const user: Nullable<DocumentUser> = await populateUser(UserModel.findOne({ email }));

        if (!user) {
          return res.status(StatusCodes.NOT_FOUND).json({
            type: 'Error',
            message: 'User not found',
          });
        }

        /** check password with bcrypt */
        const checkPassword = await bcrypt.compare(password, user.hashedPassword ?? '');

        if (!checkPassword) {
          return res.status(StatusCodes.BAD_REQUEST).json({
            type: 'Error',
            message: 'Invalid email or password',
          });
        }

        /** jwt token for auth user */
        const token = jwt.sign(
          { email },
          jwtSecret,
          { expiresIn: '24h' },
        );

        const data = user.toJSON() as IUser;
        delete data.hashedPassword;

        /** set token to cookie */
        await res.cookie('token', token, {
          httpOnly: false,
          secure: true,
        }).json({
          type: 'Result',
          data,
        });
      } catch (e) {
        res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
          type: 'Error',
          message: 'something failed',
        });
        console.error(e);
      }
    },
  );

  /** autologin */
  router.post(
    '/auto',
    auth(jwtSecret),
    async (req, res: express.Response<ResType<IUser>>) => {

      try {

        const user = req.user;

        const data = user!.toJSON() as IUser;
        delete data.hashedPassword;

        res.json({
          type: 'Result',
          data,
        });

      } catch (e) {
        res.json({
          type: 'Error',
          message: 'Server error',
        });
      }
    },
  );

  /** logout - clear cookie */
  router.delete(
    '/',
    async (req: express.Request, res: express.Response<ResType<void>>) => {
      res.clearCookie('token')
        .json({
          type: 'Result',
        });
    },
  );

  return router;
};
