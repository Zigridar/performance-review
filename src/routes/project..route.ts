import { INamed } from '../common_types/interfaces/INamed';
import { ProjectModel } from '../db/models/Poject';
import { createCRUDRouter, likeExpression } from './createRouter';

export const projectRouter = (jwtSecret: string) => {
  return createCRUDRouter<INamed>(jwtSecret, ProjectModel, {
    createFilter: ({ term }) => {
      return term ? { name: likeExpression(term) } : {};
    },
  });
};