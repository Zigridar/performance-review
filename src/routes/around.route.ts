import express, { Router } from 'express';
import { AroundDescription, ResType } from '../common_types/API_types';
import { IUser } from '../common_types/interfaces/User';
import { DocumentPerformanceReview, PerformanceReviewModel } from '../db/models/PerformanceReview';
import { isUserEmployee, populateUser, UserModel } from '../db/models/User';
import auth from '../middleware/auth.middleware';

export const aroundRouter: (jwt: string) => Router = (jwt: string) => {

  const router = Router();

  router.use(auth(jwt));

  router.get('/', async (req, res: express.Response<ResType<Array<AroundDescription>>>) => {

    try {

      const user = req.user!;

      const prs: Array<DocumentPerformanceReview> = await PerformanceReviewModel.find();

      const isEmployee = isUserEmployee(user);

      const data: Array<AroundDescription> = [];

      for (const pr of prs) {

        const [begin, end] = pr.period;

        for (const form of pr.forms) {

          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          // @ts-ignore
          if (form.users && form.users.length && (!isEmployee || form.user.includes(user.id))) {

            for (const user of form.users) {
              const formUser = (await populateUser(UserModel.findById(user)))?.toJSON() as IUser;
              delete formUser?.hashedPassword;

              data.push({
                id: formUser.id,
                begin,
                end,
                user: formUser,
              });
            }
          }

        }
      }

      await res.json({
        type: 'Result',
        data,
      });
      
    } catch (e) {
      console.error(e);
      await res.json({
        type: 'Error',
        message: 'Error',
      });
    }

  });

  return router;
};