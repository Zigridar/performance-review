import { INamed } from '../common_types/interfaces/INamed';
import { PostModel } from '../db/models/Post';
import { createCRUDRouter, likeExpression } from './createRouter';

export const postRouter = (jwtSecret: string) => {
  return createCRUDRouter<INamed>(jwtSecret, PostModel, {
    createFilter: ({ term }) => {
      return term ? { name: likeExpression(term) } : {};
    },
  });
};