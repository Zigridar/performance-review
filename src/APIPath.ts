export const APIPath = {
  auth: '/auth',
  question: '/questions',
  form: '/forms',
  pr: '/prs',
  users: '/users',
  post: '/post',
  team: '/team',
  project: '/project',
  around: '/around',
} as const;