import { config } from 'dotenv';
import path from 'path';

/** init .env variables */
config({
  path: path.resolve(__dirname, '../.env'), 
});

/** get source from env variable with assert if not provided */
const getConfigProperty: <T>(property: string) => T = <T>(property: string) => {
  const data = process.env[property] as any as T;
  if (data)
    return data;
  else
    throw new Error(`Переменаая "${property}" не указана в конфиге!`);
};

export const getConfig = () => ({
  PORT: getConfigProperty<number>('PORT'),
  JWT_SECRET: getConfigProperty<string>('JWT_SECRET'),
  MONGO_URI: getConfigProperty<string>('MONGO_URI'),
  MONGO_USER: getConfigProperty<string>('MONGO_USER'),
  MONGO_PASS: getConfigProperty<string>('MONGO_PASS'),
  INIT_USER_EMAIL: getConfigProperty<string>('INIT_USER_EMAIL'),
  INIT_USER_PASSWORD: getConfigProperty<string>('INIT_USER_PASSWORD'),
  SMTP_HOST: getConfigProperty<string>('SMTP_HOST'),
  SMTP_PORT: getConfigProperty<number>('SMTP_PORT'),
  SMTP_USER: getConfigProperty<string>('SMTP_USER'),
  SMTP_PASS: getConfigProperty<string>('SMTP_PASS'),
  SMTP_SENDER_ADDRESS: getConfigProperty<string>('SMTP_SENDER_ADDRESS'),
  MODE: getConfigProperty<string>('MODE'),
});