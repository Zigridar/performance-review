import mongoose from 'mongoose';
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import normalize from '@meanie/mongoose-to-json';

/**
 * initialize db connection
 * @param uri - db instance URI
 * @param user - login
 * @param password - user password
 * */
export const dbConnect = (uri: string, user: string, password: string) =>
  new Promise<void>((resolve, reject) => {
  /** db connection **/
    mongoose.connect(uri, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
      connectTimeoutMS: 60_000,
      //todo
      // auth: {
      //   password,
      //   user,
      // },
    }, (error) => {
      if (!error) {
        console.info(`connect to mongo on ${uri}`);
        resolve();
      } else
        reject(error);
    });
  });


export const normalizePlugin: () => void = normalize;