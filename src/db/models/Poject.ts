import { model, Model, Schema } from 'mongoose';
import { INamed } from '../../common_types/interfaces/INamed';
import { DocumentModel } from '../../common_types/TypeUtils';
import { normalizePlugin } from '../connect';
import { ModelNames } from '../model-names';

export type DocumentProject = DocumentModel<INamed>;

const ProjectSchema: Schema<DocumentProject> = new Schema<DocumentProject>({
  name: { type: String, required: true },
});

ProjectSchema.plugin(normalizePlugin);

export const ProjectModel: Model<DocumentProject> = model<DocumentProject>(ModelNames.project, ProjectSchema);