import bcrypt from 'bcrypt';
import { model, Model, Query, Schema } from 'mongoose';
import { ADMIN, EMPLOYEE, HR, IUser, LEADER } from '../../common_types/interfaces/User';
import { DocumentModel, Nullable } from '../../common_types/TypeUtils';
import { getConfig } from '../../config';
import { normalizePlugin } from '../connect';
import { ModelNames } from '../model-names';

export type DocumentUser = DocumentModel<IUser>;

const UserSchema: Schema<DocumentUser> = new Schema<DocumentUser>({
  name: { type: String, required: true },
  lastName: { type: String, required: true },
  middleName: { type: String },
  email: { type: String, required: true, unique: true },
  leader: { type: Schema.Types.ObjectId, ref: ModelNames.user },
  team: { type: Schema.Types.ObjectId, ref: ModelNames.team },
  post: { type: Schema.Types.ObjectId, ref: ModelNames.post },
  project: { type: Schema.Types.ObjectId, ref: ModelNames.project },
  hr: { type: Schema.Types.ObjectId, ref: ModelNames.user },
  phone: { type: String, required: true },
  roles: { type: [String], enum: [HR, LEADER, ADMIN, EMPLOYEE], validate: (arr: Array<string>) => arr.length > 0 },
  hashedPassword: { type: String, required: true },
});

export const UserModel: Model<DocumentUser> = model<DocumentUser>(ModelNames.user, UserSchema);

UserSchema.plugin(normalizePlugin);

export const hashPassword: (pass?: string) => Promise<string> = (pass = '') => bcrypt.hash(pass, 10);

/** creates user if db is empty */
export const createUserIfNotExists: () => Promise<Nullable<DocumentUser>> = async () => {
  const usersCount: number = await UserModel.countDocuments();
  if (!usersCount) {
    const config = getConfig();

    const user = new UserModel();
    user.email = config.INIT_USER_EMAIL;
    user.hashedPassword = await hashPassword(config.INIT_USER_PASSWORD);
    user.roles = [ADMIN]
    user.name = 'Auto';
    user.lastName = 'Auto';
    user.middleName = 'Auto';
    user.phone = '+79999999999';
    return user.save();
  } else return null;
};

/** Присоединить поля по ссылкам */
export const populateUser = <T, M>(query: Query<T, M>) => {
  return query
    .populate('post')
    .populate('team')
    .populate('hr')
    .populate('leader')
    .populate('project');
};

export const isUserLeader = (user: IUser) => user.roles.includes(LEADER);

export const isUserHR = (user: IUser) => user.roles.includes(HR);

export const isUserEmployee = (user: IUser) => user.roles.includes(EMPLOYEE) && !isUserLeader(user) && !isUserHR(user);

