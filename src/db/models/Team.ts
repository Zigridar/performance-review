import { model, Model, Schema } from 'mongoose';
import { INamed } from '../../common_types/interfaces/INamed';
import { DocumentModel } from '../../common_types/TypeUtils';
import { normalizePlugin } from '../connect';
import { ModelNames } from '../model-names';

export type DocumentTeam = DocumentModel<INamed>;

const TeamSchema: Schema<DocumentTeam> = new Schema<DocumentTeam>({
  name: { type: String, required: true },
});

TeamSchema.plugin(normalizePlugin);

export const TeamModel: Model<DocumentTeam> = model<DocumentTeam>(ModelNames.team, TeamSchema);