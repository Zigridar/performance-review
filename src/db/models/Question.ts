import { Document, model, Model, Schema } from 'mongoose';
import { CLOSED, IAnswer, IQuestion, OPEN } from '../../common_types/interfaces/Question';
import { DocumentModel } from '../../common_types/TypeUtils';
import { normalizePlugin } from '../connect';
import { ModelNames } from '../model-names';

export type DocumentQuestion = DocumentModel<IQuestion>;

const AnswerSchema: Schema<Document<IAnswer>> = new Schema<Document<IAnswer>>({
  text: { type: String, required: true },
  isCorrect: { type: Boolean, required: true, default: false },
});

export const QuestionSchema: Schema<DocumentQuestion> = new Schema<DocumentQuestion>({
  text: { type: String, required: true },
  type: { type: String, required: true, enum: [CLOSED, OPEN] },
  answers: { type: [AnswerSchema], validate: (arr: Array<typeof AnswerSchema>) => arr.length > 0 },
  reviewTypes: [String],
});

QuestionSchema.plugin(normalizePlugin);
AnswerSchema.plugin(normalizePlugin);

export const QuestionModel: Model<DocumentQuestion> = model<DocumentQuestion>(ModelNames.question, QuestionSchema);
