import { model, Model, Schema } from 'mongoose';
import { IPerformanceReview } from '../../common_types/interfaces/PerformanceReview';
import { DocumentModel } from '../../common_types/TypeUtils';
import { normalizePlugin } from '../connect';
import { ModelNames } from '../model-names';
import { FormSchema } from './Form';

export type DocumentPerformanceReview = DocumentModel<IPerformanceReview>;

const PerformanceReviewSchema: Schema<DocumentPerformanceReview> = new Schema<DocumentPerformanceReview>({
  author: { type: Schema.Types.ObjectId, ref: ModelNames.user, required: true },
  employee: { type: Schema.Types.ObjectId, ref: ModelNames.user, required: true },
  period: [{ type: Number, required: true }],
  forms: [FormSchema],
});

PerformanceReviewSchema.plugin(normalizePlugin);

export const PerformanceReviewModel: Model<DocumentPerformanceReview> = model<DocumentPerformanceReview>(ModelNames.performanceReview, PerformanceReviewSchema);
