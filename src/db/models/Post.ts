import { model, Model, Schema } from 'mongoose';
import { INamed } from '../../common_types/interfaces/INamed';
import { DocumentModel } from '../../common_types/TypeUtils';
import { normalizePlugin } from '../connect';
import { ModelNames } from '../model-names';

export type DocumentPost = DocumentModel<INamed>;

const PostSchema: Schema<DocumentPost> = new Schema<DocumentModel<INamed>>({
  name: { type: String, required: true, trim: true },
});

PostSchema.plugin(normalizePlugin);

export const PostModel: Model<DocumentPost> = model<DocumentPost>(ModelNames.post, PostSchema);