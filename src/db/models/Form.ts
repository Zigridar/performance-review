import { model, Model, Schema } from 'mongoose';
import { IForm, IQuestionInfo, IRate } from '../../common_types/interfaces/Form';
import { DocumentModel } from '../../common_types/TypeUtils';
import { normalizePlugin } from '../connect';
import { ModelNames } from '../model-names';
import { QuestionSchema } from './Question';

export type DocumentForm = DocumentModel<IForm>;

const QuestionInfoSchema: Schema<DocumentModel<IQuestionInfo>> = new Schema<DocumentModel<IQuestionInfo>>({
  question: { type: QuestionSchema, required: true, trim: true },
  rate: { type: Number, required: true },
});

const RateSchema: Schema<DocumentModel<IRate>> = new Schema<DocumentModel<IRate>>({
  scoreFrom: { type: Number, required: true },
  scoreTo: { type: Number, required: true },
  mark: { type: String, required: true },
});

export const FormSchema: Schema<DocumentForm> = new Schema<DocumentForm>({
  description: { type: String, required: true },
  type: { type: String, required: true },
  questions: [QuestionInfoSchema],
  graded: { type: Boolean, required: true, default: false },
  rating: [RateSchema],
  users: [{ type: String }],
});

FormSchema.plugin(normalizePlugin);

export const FormModel: Model<DocumentForm> = model<DocumentForm>(ModelNames.form, FormSchema);
