export const ModelNames = {
  form: 'form',
  performanceReview: 'performance-review',
  post: 'post',
  question: 'question',
  user: 'user',
  team: 'team',
  project: 'project',
} as const;