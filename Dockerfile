FROM node:14-alpine
COPY package.json package-lock.json ./app/
COPY ./dist  ./app/dist/
RUN cd ./app && npm i --production
EXPOSE 8080
CMD cd ./app && npm start
