interface IClassName {
  [className: string]: string
}

declare module '*.module.less' {
  const classes: IClassName;
  export = classes;
}

declare module '*.module.scss' {
  const classes: IClassName;
  export = classes;
}

declare module '*.module.sass' {
  const classes: IClassName;
  export = classes;
}