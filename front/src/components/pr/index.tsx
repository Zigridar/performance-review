import { PlusOutlined } from '@ant-design/icons';
import { Button, Checkbox, Collapse, DatePicker, Form, Modal, Typography } from 'antd';
import { useForm } from 'antd/es/form/Form';
import { ColumnsType } from 'antd/lib/table';
import moment from 'moment';
import React, { useEffect, useState } from 'react';
import { nameof as nameOf } from 'ts-simple-nameof';
import { IForm } from '../../../../src/common_types/interfaces/Form';
import { IPerformanceReview } from '../../../../src/common_types/interfaces/PerformanceReview';
import { AROUND, ATTESTATION, ReviewType, reviewTypes } from '../../../../src/common_types/interfaces/Review';
import { IUser } from '../../../../src/common_types/interfaces/User';
import { createLoadItems, doWithResponse, FormsAPI, PrAPI } from '../../api';
import { useAuth } from '../../auth';
import { reviewTypeToString, simpleRequired } from '../../constants/maps';
import { useUpdate } from '../../hooks/useUpdate';
import { createPRFx, deletePRFx } from '../../models/pr';
import { formatFIO } from '../../utils';
import { Collection, FieldSetProps } from '../base/collection';
import { DynamicFormList } from '../base/form-list';
import { AutoSuggest } from '../base/select';
import { UserSelect } from '../users/UserSelect';
import classes from './style.module.less';

const { Panel } = Collapse;
const { Text } = Typography;

/** Колонки таблицы PR */
const columns: ColumnsType<IPerformanceReview> = [
  {
    title: 'ФИО Сотрудника',
    key: 'employee',
    dataIndex: nameOf<IPerformanceReview>(pr => pr.employee),
    render: (user: IUser) => {
      return (<Text>{formatFIO(user)}</Text>);
    },
  },
  {
    title: 'Дата начала',
    key: 'begin',
    dataIndex: nameOf<IPerformanceReview>(pr => pr.period),
    render: ([begin]: [number, number]) => {
      return (<Text>{new Date(begin).toLocaleDateString()}</Text>);
    },
  },
  {
    title: 'Дата окончания',
    key: 'end',
    dataIndex: nameOf<IPerformanceReview>(pr => pr.period),
    render: ([, end]: [number, number]) => {
      return (<Text>{new Date(end).toLocaleDateString()}</Text>);
    },
  },
];

/** Колонки таблиц с анкетами */
const formColumns: ColumnsType<IForm> = [
  {
    title: 'Наименовани анкеты',
    key: nameOf<IForm>(f => f.description),
    render(_, item) {
      return item.description;
    },
  },
];

type FormCollectionProps = {
  items: Array<IForm>;
  formType: ReviewType;
};

/** Коллекция форм определенного типа */
const FormCollection: React.FC<FormCollectionProps> = ({ items, formType }) => {

  /** Копия колонок */
  const columns = [...formColumns];

  /** Добавление специфичных */
  if (formType !== AROUND) {
    columns.push(
      {
        title: 'Количество вопросов',
        key: nameOf<IForm>(f => f.questions),
        render(_, item) {
          return item?.questions.length ?? 0;
        },
      },
    );
  } else {
    columns.push(
      {
        title: 'Количество сотрудников',
        key: nameOf<IForm>(f => f.questions),
        render(_, item) {
          return item?.users?.length ?? 0;
        },
      },
    );
  }

  if (formType === ATTESTATION) {
    columns.push(
      {
        title: 'Оцениваемая',
        key: nameOf<IForm>(f => f.graded),
        render(_, item) {
          return <Checkbox checked={item.graded}/>;
        },
      },
    );
  }

  return (
    <Collection
      //todo pagination
      useOnlyItems={true}
      withTextSearch={false}
      tableClassName=''
      columns={columns}
      items={items}
    />
  );
};

type AddButtonProps = {
  onAdd: () => void;
};

/** Кнопка добавления формы в заголовке аккордиона */
const AddButton: React.FC<AddButtonProps> = ({ onAdd }) => {

  const onClick = (e: React.MouseEvent<HTMLDivElement>) => {
    onAdd();
    e.stopPropagation();
  };

  return (
    <Button
      icon={<PlusOutlined/>}
      shape='circle'
      size='small'
      className={classes.button}
      onClick={onClick}
      type='primary'
    />
  );
};

type AddFormModalProps = {
  onCancel: () => void;
  onSelectForm: (form: IForm) => void;
  formType: ReviewType;
};

/** Вспомогательныф тип для выбора формы */
type SelectForm = {
  form: IForm;
  users?: Array<IUser>;
};

/** Диалог выбора формы */
const AddFormModal: React.FC<AddFormModalProps> = ({ formType, onCancel, onSelectForm }) => {

  /** Поиск форм с учетом типа */
  const searchItems = (term: string) => FormsAPI.get({ term, additionalFilters: { formType } }).then(res => (doWithResponse(res, {
    onResult: res => res.items,
  })));

  /** Используется для влидации выбора */
  const [form] = useForm<SelectForm>();

  /** Отмена */
  const cancel = () => {
    form.resetFields();
    onCancel();
  };

  /** Выбрать */
  const onOk = () => {
    /** Валидация */
    form.validateFields()
      .then(({ form, users }) => {
        if (formType === AROUND && (!users || !users.length)) {
          Modal.error({
            title: 'Ошибка',
            content: 'Требуется выбрать сотрудников',
          });
        } else {
          form.users = users;
          onSelectForm(form);
          cancel();
        }
      })
      .catch(() => {});
  };

  return (
    <Modal
      visible={!!formType}
      destroyOnClose
      onCancel={cancel}
      onOk={onOk}
      title='Выбор анкеты'
      cancelButtonProps={{ className: classes.button }}
      okButtonProps={{ className: classes.button }}
    >
      <Form form={form} layout='vertical'>
        <Form.Item
          label='Анкета'
          rules={[simpleRequired()]}
          name={nameOf<SelectForm>(s => s.form)}
        >
          <AutoSuggest<IForm>
            optionContent={({ item }) => <>{item?.description}</>}
            getValue={item => item?.id}
            searchItems={searchItems}
            //todo shit
            useSearchCache={false}
          />
        </Form.Item>
        {formType === AROUND &&
          <>
            <Text>Сотрудники</Text>
            <DynamicFormList
              name={nameOf<SelectForm>(s => s.users)}
            >
              {({ name }) => (
                <Form.Item
                  name={name}
                  rules={[simpleRequired()]}
                  className={classes.flexMargin}
                >
                  <UserSelect/>
                </Form.Item>
              )}
            </DynamicFormList>
          </>
        }
      </Form>
    </Modal>
  );
};

const formsFieldName = nameOf<IPerformanceReview>(pr => pr.forms);

/** Форма для создания Performance Review */
const PRFieldSet: React.FC<FieldSetProps<IPerformanceReview>> = ({ isInView, current, form }) => {

  /** Выбор типа формы в текущий момент */
  const [formType, setFormType] = useState<ReviewType>(null);

  const update = useUpdate();

  /** Начало выбора формы */
  const onBeginSelect = (formType: ReviewType) => {
    setFormType(formType);
  };

  /** Отмена выбора формы */
  const onCancel = () => {
    setFormType(null);
  };

  /** Форма выбрана */
  const onSelectForm = (selectedForm: IForm) => {
    const forms = form.getFieldValue(formsFieldName) as Array<IForm>;
    form.setFields([
      {
        name: formsFieldName,
        value: [...forms, selectedForm],
        touched: true,
        errors: [],
        validating: true,
        warnings: [],
      },
    ]);
    /** Принудительное обновление, т.к. не было грамотного изменения */
    update();
  };

  /** Получение анкет нужного типа */
  const getForms = (formType: ReviewType) => {
    const res = form.getFieldValue(nameOf<IPerformanceReview>(pr => pr.forms));
    return ((res || []) as Array<IForm>).filter(item => item.type === formType);
  };

  /** При инициализации установить формы пустым массивом */
  useEffect(() => {
    const res = form.getFieldValue(nameOf<IPerformanceReview>(pr => pr.forms));
    if (!res) {
      form.setFields([
        {
          name: nameOf<IPerformanceReview>(pr => pr.forms),
          value: current?.forms || [],
        },
      ]);
    }
  }, []);

  return (
    <>
      <Form.Item
        label='Период проведения'
        rules={[simpleRequired()]}
        name={nameOf<IPerformanceReview>(pr => pr.period)}
        initialValue={current?.period}
      >
        <DatePicker.RangePicker disabled={isInView} className={classes.fullWidth}/>
      </Form.Item>
      <Form.Item
        label='Сотрудник'
        rules={[simpleRequired()]}
        initialValue={current?.employee}
        name={nameOf<IPerformanceReview>(pr => pr.employee)}
      >
        <UserSelect isInView={isInView}/>
      </Form.Item>
      <Form.Item label='Анкеты'>
        <AddFormModal
          onCancel={onCancel}
          onSelectForm={onSelectForm}
          formType={formType}
        />
        <Collapse className={classes.customPadding} accordion>
          {reviewTypes().map(reviewType => (
            <Panel
              header={reviewTypeToString(reviewType)}
              key={reviewType}
              extra={!isInView && <AddButton onAdd={() => onBeginSelect(reviewType)}/>}
            >
              <FormCollection
                formType={reviewType}
                items={getForms(reviewType)}
              />
            </Panel>
          ))
          }
        </Collapse>
      </Form.Item>
    </>
  );
};

type PRsProps = {
  isInCreate: boolean;
  onFinish: () => void;
  actual: boolean;
};

/**
 * Реестр PR
 * */
export const PRs: React.FC<PRsProps> = ({ isInCreate, onFinish, actual }) => {

  const { user } = useAuth();

  /** Создание PR */
  const onFinishCreate = (pr: IPerformanceReview) => {
    onFinish();
    pr.author = user;
    const [begin, end] = pr.period;
    pr.period = [begin.valueOf(), end.valueOf()];
    return createPRFx(pr);
  };

  /** Удаление PR */
  const onDelete = (pr: IPerformanceReview) => {
    return deletePRFx(pr);
  };

  const validator = (pr: IPerformanceReview) => {
    if (!pr.forms?.length)
      return 'Анкеты не выбраны';
    return null;
  };

  const mapItem = (pr: IPerformanceReview) => {
    const [begin, end] = pr.period;
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    pr.period = [moment(begin), moment(end)];
    return pr;
  };

  return (
    <Collection
      validator={validator}
      loadItems={filter => createLoadItems(PrAPI)({ ...filter, additionalFilters: { actual } })}
      columns={columns}
      mapItem={mapItem}
      canView
      canCreate
      canDelete
      onCancel={onFinish}
      externalCreate={{ isInCreate }}
      withTextSearch={false}
      withPagination={false}
      tableClassName=''
      onFinishCreate={onFinishCreate}
      onDelete={onDelete}
      filedSet={PRFieldSet}
      modalWidth='800px'
      itemTitle={() => 'Performance Review'}
    />
  );
};

