import { Checkbox, Divider, Form, Input, InputNumber, Select, Tag, Typography } from 'antd';
import { ColumnsType } from 'antd/lib/table';
import React, { useState } from 'react';
import { nameof } from 'ts-simple-nameof';
import { IForm, IQuestionInfo, IRate } from '../../../../src/common_types/interfaces/Form';
import { IQuestion } from '../../../../src/common_types/interfaces/Question';
import { ReviewType, reviewTypes } from '../../../../src/common_types/interfaces/Review';
import { IUser } from '../../../../src/common_types/interfaces/User';
import { createLoadItems, FormsAPI, QuestionsAPI, searchById } from '../../api';
import { getReviewTag, reviewTypeToString, simpleRequired } from '../../constants/maps';
import { createFormFx, deleteFormFx, editFormFx } from '../../models/form';
import { deepCopy } from '../../utils';
import { Collection, FieldSetProps } from '../base/collection';
import { DynamicFormList } from '../base/form-list';
import { AutoSuggest } from '../base/select';

import classes from './forms.module.less';

const { Paragraph } = Typography;

interface OwnProps {
  forms?: IForm[];
  deleteForm?: (form: IForm) => Promise<IForm>;
  canCreate?: boolean;
  canDelete?: boolean;
  canView?: boolean;
  canEdit?: boolean;
  withPadding?: boolean;
  withHeader?: boolean;
  itemPopup?: boolean;
  prEdit?: boolean;
  withURLSearch?: boolean;
  withURLPagination?: boolean;
  prUsersSelect?: (users: IUser[], form: IForm) => void;
}

type Props = OwnProps;

const columns: ColumnsType<IForm> = [
  {
    dataIndex: nameof<IForm>(f => f.description),
    key: nameof<IForm>(f => f.description),
    width: '40%',
    title: 'Наименование анкеты',
  },
  {
    dataIndex: nameof<IForm>(f => f.type),
    key: nameof<IForm>(f => f.type),
    width: '30%',
    title: 'Тип анкеты',
    render: (reviewType: ReviewType) => {
      const tag = getReviewTag(reviewType);
      return (<Tag key={tag.type} color={tag.color}>{tag.title}</Tag>);
    },
  },
  {
    dataIndex: nameof<IForm>(f => f.author),
    key: nameof<IForm>(f => f.author),
    title: 'Автор анкеты',
    render: (authorId: string) => {
      return (<>{authorId}</>);
    },
  },
  {
    title: 'Дата последнего изменения',
    dataIndex: nameof<IForm>(f => f.modifyDate),
    key: nameof<IForm>(f => f.modifyDate),
    render: (date: number) => {
      const d = new Date(date);
      return (<>{d.toLocaleString()}</>);
    },
  },
];


/** Карточка формы */
const FormFieldSet: React.FC<FieldSetProps<IForm>> = (props) => {

  const { current: form, isInView } = props;

  const [graded, setGraded] = useState<boolean>(true);

  const changeGraded = () => {
    setGraded(prev => !prev);
  };

  const searchQuestions = (text: string) => createLoadItems(QuestionsAPI)({ term: text }).then(res => res.items);

  return (
    <>
      <Form.Item
        label='Наименование анкеты'
        name={nameof<IForm>(f => f.description)}
        initialValue={form?.description}
        rules={[simpleRequired()]}
      >
        <Input disabled={isInView}/>
      </Form.Item>
      <Form.Item
        label='Тип анкеты'
        name={nameof<IForm>(f => f.type)}
        initialValue={form?.type}
        rules={[simpleRequired()]}
      >
        <Select disabled={isInView}>
          {reviewTypes().map(type => (
            <Select.Option key={type} value={type}>{reviewTypeToString(type)}</Select.Option>
          ))}
        </Select>
      </Form.Item>
      <Form.Item
        labelAlign={'left'}
        name={nameof<IForm>(f => f.graded)}
        valuePropName='checked'
        initialValue={form?.graded ?? graded}
      >
        <Checkbox
          checked={graded}
          className={classes.rowReverse}
          onChange={changeGraded}
        >
          Оцениваемая
        </Checkbox>
      </Form.Item>
        {graded &&
          <>
            <Divider/>
            <Paragraph className={classes.header}>
              Бально-рейтинговая система
            </Paragraph>
            <DynamicFormList
              name={nameof<IForm>(f => f.rating)}
              initialValue={form?.rating ?? []}
              isInView={isInView}
            >
              {
                ({  name }) => {
                  return (
                    <>
                      <Form.Item
                        name={[name, nameof<IRate>(r => r.mark)]}
                        label='Отметка'
                        rules={[simpleRequired()]}
                        className={classes.flex}
                      >
                        <Input disabled={isInView} type='text'/>
                      </Form.Item>
                      <Form.Item
                        name={[name, nameof<IRate>(r => r.scoreFrom)]}
                        label='От'
                        rules={[simpleRequired()]}
                      >
                        <InputNumber disabled={isInView}/>
                      </Form.Item>
                      <Form.Item
                        name={[name, nameof<IRate>(r => r.scoreTo)]}
                        label='До'
                        rules={[simpleRequired()]}
                      >
                        <InputNumber disabled={isInView}/>
                      </Form.Item>
                    </>
                  );
                }
              }
            </DynamicFormList>
          </>
        }
        <Divider/>
        <Paragraph className={classes.header}>
          Вопросы
        </Paragraph>
        <DynamicFormList
          isInView={isInView}
          name={nameof<IForm>(f => f.questions)}
          initialValue={form?.questions ?? []}
        >
          {
            ({ name }) => {
              return (
                <>
                  <Form.Item
                    label='Вопрос'
                    name={[name, nameof<IQuestionInfo>(q => q.question)]}
                    rules={[simpleRequired()]}
                    style={{ flex: 1 }}
                  >
                    <AutoSuggest<IQuestion>
                      disabled={isInView}
                      getValue={q => q?.id}
                      searchItems={searchQuestions}
                      optionContent={q => <>{q.item.text}</>}
                    />
                  </Form.Item>
                  <Form.Item
                    label='Балл'
                    name={[name, nameof<IQuestionInfo>(q => q.rate)]}
                    rules={[simpleRequired()]}
                  >
                    <InputNumber disabled={isInView}/>
                  </Form.Item>
                </>
              );
            }
          }
        </DynamicFormList>
    </>
  );
};

/**
 * Коллекция анкет
 * */
export const Forms: React.FC<Props> = (props) => {

  const onDelete = (form: IForm) => {
    return deleteFormFx(form);
  };

  const onFinishEdit = (form: IForm) => {
    const copy = deepCopy(form);
    return editFormFx(copy);
  };

  const onFinishCreate = (form: IForm) => {
    const copy = deepCopy(form);
    return createFormFx(copy);
  };

  const validator = (form: IForm) => {
    if (form.graded && form.rating.length < 1)
      return 'БРС не заполнена';
    else if (form.questions.length < 1)
      return 'Вопросы не выбраны';
    else
      return null;
  };

  return (
    <Collection
      withURLSearch={true}
      withURLPagination={true}
      loadItems={createLoadItems(FormsAPI)}
      searchItemById={searchById(FormsAPI)}
      columns={columns}
      itemTitle={() => 'Анкета'}
      filedSet={FormFieldSet}
      canDelete={true}
      canEdit={true}
      canView={true}
      canCreate={true}
      onDelete={onDelete}
      onFinishEdit={onFinishEdit}
      onFinishCreate={onFinishCreate}
      validator={validator}
      modalWidth={'700px'}
      style={ { padding: '30px' } }
    />
  );
};
