import { Button, Empty, Spin } from 'antd';
import React, { useEffect, useState } from 'react';
import { IPerformanceReview } from '../../../../src/common_types/interfaces/PerformanceReview';
import { doWithResponse, PrAPI } from '../../api';
import classes from './user-page.module.less';

/** Кнопка PR отображаемая для сотрудника */
export const EmployeePR: React.FC = () => {

  const [loading, setLoading] = useState(false);

  const [pr, setPr] = useState<IPerformanceReview>();

  /** Загрузка предстоящего PR */
  useEffect(() => {
    setLoading(true);
    PrAPI
      .get()
      .then(res => doWithResponse(res, {
        onResult: result => setPr(result.items[0]),
      }))
      .finally(() => setLoading(false));
  }, []);

  /** Открытие карточки PR */
  const onOpenPR = () => {
    //todo
  };

  return (
    <div className={classes.employeePrContainer}>
      {loading ?
        <Spin size='large'/>
        :
        pr ?
        <Button
          type='link'
          onClick={onOpenPR}
        >
          todo pr link
        </Button>
          :
        <Empty/>
      }
    </div>
  );
};