import { Button } from 'antd';
import React, { useState } from 'react';
import { PRs } from '../pr';
import classes from './user-page.module.less';

/** Коллекция отображаемая тем, у кого много прав */
export const PRCollectionWithExternalCreate: () => [JSX.Element, JSX.Element] = () => {

  /** Создать PR */
  const [isInCreate, setIsInCreate] = useState<boolean>(false);

  /** Начало создания */
  const onCreate = (e: React.MouseEvent<HTMLDivElement>) => {
    e.stopPropagation();
    setIsInCreate(true);
  };

  /** Кнопка создания */
  const createButton = (
    <Button
      onClick={onCreate}
      type='primary'
      className={classes.create}
    >
      Добавить
    </Button>
  );

  const collection = (
    <PRs
      actual={true}
      isInCreate={isInCreate}
      onFinish={() => setIsInCreate(false)}
    />
  );

  return [collection, createButton];
};