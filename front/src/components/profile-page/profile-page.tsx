import { Collapse } from 'antd';
import React from 'react';
import { EMPLOYEE, HR, LEADER } from '../../../../src/common_types/interfaces/User';
import { useAuth } from '../../auth';
import { LeftRightPanel } from '../left-right';
import { UserCard } from '../user-content/UserCard';
import { AroundCollection } from './around-collection';
import { DonePRs } from './done-prs';
import { EmployeePR } from './employee-pr';
import { PRCollectionWithExternalCreate } from './pr-collection-with-external-create';
import classes from './user-page.module.less';

const { Panel } = Collapse;

/** Профиль пользователя  */
export const ProfilePage: React.FC = () => {

  const { user } = useAuth();

  /** Простой сотрудник */
  const isEmployee = user.roles.includes(EMPLOYEE) && !user.roles.includes(LEADER) && !user.roles.includes(HR);

  const [prCollection, createButton] = PRCollectionWithExternalCreate();

  const left = (
    <Collapse
      className={classes.fullHeight}
      accordion
    >
      <Panel
        key={1}
        header={`${isEmployee ? 'Предстоящее' : 'Предстоящие'} Performance Review`}
        extra={!isEmployee && createButton}
      >
        {isEmployee ?
          <EmployeePR/>
          :
          prCollection
        }
      </Panel>
      <Panel
        key={2}
        header={'Анкеты 360, ожидающие прохождения'}
      >
        <AroundCollection/>
      </Panel>
      <Panel
        key={3}
        header={'Прошедшие Performance Review'}
      >
        <DonePRs/>
      </Panel>
    </Collapse>
  );

  const right = (<UserCard/>);

  return (
    <LeftRightPanel
      left={left}
      right={right}
    />
  );
};