import { ColumnsType } from 'antd/es/table';
import React, { useEffect, useState } from 'react';
import { AroundDescription } from '../../../../src/common_types/API_types';
import { apiInstance } from '../../api';
import { formatFIO } from '../../utils';
import { Collection } from '../base/collection';

const columns: ColumnsType<AroundDescription> = [
  {
    title: 'ФИО сотрудника',
    key: 'fio',
    render: (_, item) => {
      return (formatFIO(item.user));
    },
  },
  {
    title: 'Дата начала',
    key: 'begin',
    render: (_, item) => {
      return new Date(item.begin).toLocaleDateString();
    },
  },
  {
    title: 'Дата конца',
    key: 'end',
    render: (_, item) => {
      return new Date(item.end).toLocaleDateString();
    },
  },
];

export const AroundCollection: React.FC = () => {

  const [items, setItems] = useState<Array<AroundDescription>>([]);

  useEffect(() => {
    apiInstance
      .get('around')
      .then(res => {
        const data = res.data.data as Array<AroundDescription>;
        debugger;
        setItems(data);
      });
  }, []);

  return (
    <Collection
      useOnlyItems
      items={items}
      tableClassName={''}
      columns={columns}
      withTextSearch={false}
      withPagination={false}
    />
  );
};