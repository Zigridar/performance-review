import React from 'react';
import { PRs } from '../pr';

export const DonePRs: React.FC = () => (
  <PRs
    isInCreate={false}
    onFinish={null}
    actual={false}
  />
);