import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { ADMIN, HR, LEADER } from '../../../../src/common_types/interfaces/User';
import { useAuth } from '../../auth';
import { AdminPage } from '../admin-page';
import { Forms } from '../forms';
import { Logo } from '../logo';
import { MainLayout } from '../main-layout';
import { IMenuItem, MainLinks } from '../main-links';
import { ProfilePage } from '../profile-page';
import { Questions } from '../questions';
import { UserContent } from '../user-content/UserContent';

export const MainPage: React.FC = () => {

  const { user } = useAuth();

  const menuItems: IMenuItem[] = [];

  /** Добавление вкладки Анкет и вопросов */
  if (user.roles.includes(HR) || user.roles.includes(LEADER)) {
    menuItems.push(
      {
        menuContent: 'Анкеты',
        content: <Forms/>,
        path: '/forms',
      },
      {
        menuContent: 'Вопросы',
        content: <Questions/>,
        path: '/questions',
      },
    );
  }

  /** Добавление администрирования */
  if (user.roles.includes(ADMIN)) {
    menuItems.push({
      menuContent: 'Администрирование',
      content: <AdminPage/>,
      path: '/admin',
    });
  }

  return (
    <Switch>
      <MainLayout
        headerLinks={<MainLinks menuItems={menuItems}/>}
        logoContent={<Logo/>}
        userContent={<UserContent/>}
      >
        <Switch>
          {menuItems.map((item, index) => (
            <Route key={index} path={item.path}>
              {item.content}
            </Route>
          ),
          )}
          <Route path='/profile'>
            <ProfilePage/>
          </Route>
          <Route>
            <Redirect to={'/profile'}/>
          </Route>
        </Switch>
      </MainLayout>
    </Switch>
  );
};
