import { Button, Card, Col, Form, FormInstance, Input, Row, Typography } from 'antd';
import { useForm } from 'antd/es/form/Form';
import React, { CSSProperties, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { nameof } from 'ts-simple-nameof';
import { AuthBody } from '../../../../src/common_types/API_types';
import { simpleRequired } from '../../constants/maps';

const itemStyle: CSSProperties = {
  padding: '10px 0 10px 0',
};

//todo
const backColor = '#f3f4f8';

const { Paragraph } = Typography;

interface AuthPageProps {
  login: (credentials: AuthBody) => Promise<void>;
  loading: boolean;
}

/** Login form */
export const AuthPage: React.FC<AuthPageProps> = ({ loading, login }) => {

  const [form]: [FormInstance<AuthBody>] = useForm<AuthBody>();

  const history = useHistory();

  /** Очистка URL при выходе */
  useEffect(() => {
    history.replace('');
  }, []);

  const validateLogin = () => {
    form.validateFields()
      .then(login)
      .catch(() =>{
        //todo
      });
  };

  return (
    <Row
      className="full-height"
      justify={'space-around'}
      align={'middle'}
    >
      <Col span={5}>
        <Card
          bodyStyle={{
            backgroundColor: backColor,
          }}
        >
          <Row
            className="full-height"
            justify={'space-around'}
            align={'middle'}
          >
            <Form
              form={form}
              style={{
                paddingTop: '20px',
              }}
            >
              <Paragraph style={{ textAlign: 'center', fontSize: '25px' }}>Вход</Paragraph>
              <Form.Item
                style={itemStyle}
                name={nameof<AuthBody>(a => a.email)}
                rules={[simpleRequired()]}
              >
                <Input disabled={loading}/>
              </Form.Item>

              <Form.Item
                style={itemStyle}
                name={nameof<AuthBody>(a => a.password)}
                rules={[simpleRequired()]}
              >
                <Input.Password disabled={loading}/>
              </Form.Item>
              <Form.Item
                style={itemStyle}
              >
                <Button
                  style={{
                    width: '100%',
                  }}
                  type='primary'
                  onClick={validateLogin}
                  disabled={loading}
                >
                  Войти
                </Button>
              </Form.Item>
            </Form>
          </Row>
        </Card>
      </Col>
    </Row>
  );
};
