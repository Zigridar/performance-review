import { Form, Input, Select, Tag } from 'antd';
import { ColumnsType } from 'antd/lib/table';
import React from 'react';
import { nameof } from 'ts-simple-nameof';
import { IUser, UserRole, userRoles } from '../../../../src/common_types/interfaces/User';
import { createLoadItems, PostAPI, ProjectAPI, searchById, TeamAPI, UsersAPI } from '../../api';
import { getRoleTag, simpleRequired, userRoleToString } from '../../constants/maps';
import { createUserFx, deleteUserFx, editUserFx } from '../../models/user';
import { deepCopy } from '../../utils';
import { Collection, FieldSetProps } from '../base/collection';
import { DictionarySelect } from '../dictionary';
import { UserSelect } from './UserSelect';

const { Option } = Select;

const columns: ColumnsType<IUser> = [
  {
    title: 'Фамилия',
    dataIndex: 'lastName',
  },
  {
    title: 'Имя',
    dataIndex: 'name',
  },
  {
    title: 'Отчество',
    dataIndex: 'middleName',
  },
  {
    title: 'Телефон',
    dataIndex: 'phone',
  },
  {
    title: 'email',
    dataIndex: 'email',
  },
  // {
  //   title: 'leader',
  //   dataIndex: 'leaderId',
  //   render: (leaderId: string) => (<Text>{userIndex.get(leaderId)?.lastName ?? 'отсутствует'}</Text>),
  // },
  {
    title: 'Роли',
    dataIndex: 'roles',
    render: (roles: UserRole[]) => {
      return (
        <>
          {roles.map(getRoleTag).map(tag => (<Tag key={tag.type} color={tag.color}>{tag.title}</Tag>))}
        </>
      );
    },
  },
];

/** Форма пользователя */
const UserFieldSet: React.FC<FieldSetProps<IUser>> = ({ isInView, current: user }) => {

  return (
    <>
      <Form.Item
        name={nameof<IUser>(u => u.name)}
        label='Имя'
        initialValue={user?.name}
        rules={[simpleRequired()]}
      >
        <Input disabled={isInView} />
      </Form.Item>
      <Form.Item
        name={nameof<IUser>(u => u.lastName)}
        label='Фамилия'
        initialValue={user?.lastName}
        rules={[simpleRequired()]}
      >
        <Input disabled={isInView} />
      </Form.Item>
      <Form.Item
        name={nameof<IUser>(u => u.middleName)}
        label={'Отчество'}
        initialValue={user?.middleName}
      >
        <Input disabled={isInView} />
      </Form.Item>
      <Form.Item
        name={nameof<IUser>(u => u.phone)}
        label={'Мобильный телефон'}
        initialValue={user?.phone}
        rules={[
          simpleRequired(),
          {
            message: 'Введите телефон',
            pattern: new RegExp(/^\+7[489][0-9]{2}[0-9]{3}[0-9]{2}[0-9]{2}$/gm),
          },
        ]
        }
      >
        <Input disabled={isInView} placeholder={'+79999999999'} />
      </Form.Item>
      <Form.Item
        name={nameof<IUser>(u => u.email)}
        label={'E-mail'}
        initialValue={user?.email}
        rules={[
          simpleRequired(),
          {
            type: 'email',
            message: 'Введите email',
          },
        ]
        }
      >
        <Input disabled={isInView} />
      </Form.Item>
      <Form.Item
        name={nameof<IUser>(u => u.leader)}
        label='Руководитель'
        initialValue={user?.leader}
      >
        <UserSelect isInView={isInView}/>
      </Form.Item>
      <Form.Item
        name={nameof<IUser>(u => u.hr)}
        label='HR'
        initialValue={user?.hr}
      >
        <UserSelect isInView={isInView}/>
      </Form.Item>
      <Form.Item
        name={nameof<IUser>(u => u.post)}
        rules={[simpleRequired()]}
        label='Должность'
        initialValue={user?.post}
      >
        <DictionarySelect isInView={isInView} crud={PostAPI}/>
      </Form.Item>
      <Form.Item
        name={nameof<IUser>(u => u.team)}
        rules={[simpleRequired()]}
        initialValue={user?.team}
        label='Команда'
      >
        <DictionarySelect isInView={isInView} crud={TeamAPI}/>
      </Form.Item>
      <Form.Item
        name={nameof<IUser>(u => u.project)}
        rules={[simpleRequired()]}
        initialValue={user?.project}
        label='Проект'
      >
        <DictionarySelect isInView={isInView} crud={ProjectAPI}/>
      </Form.Item>
      <Form.Item
        name={nameof<IUser>(u => u.roles)}
        label='Роли'
        initialValue={user?.roles}
        rules={[simpleRequired()]}
      >
        <Select
          mode={'multiple'}
          allowClear={true}
          disabled={isInView}
          size={'small'}
        >
          {userRoles().map(role => (
            <Option key={role} value={role}>{userRoleToString(role)}</Option>
          ))}
        </Select>
      </Form.Item>
      {!user?.id &&
        <Form.Item
          name={nameof<IUser>(u => u.hashedPassword)}
          label='Пароль'
          rules={[simpleRequired()]}
        >
          <Input.Password disabled={isInView} autoComplete={'new-password'} />
        </Form.Item>
      }
    </>
  );
};

/** Коллекция пользователей */
export const Users: React.FC = () => {

  const onDelete = (user: IUser) => {
    return deleteUserFx(user);
  };

  const onFinishEdit = (user: IUser) => {
    const copy = deepCopy(user);
    return editUserFx(copy);
  };

  const onFinishCreate = (user: IUser) => {
    const copy = deepCopy(user);
    return createUserFx(copy);
  };

  return (
    <Collection
      modalWidth='700px'
      loadItems={createLoadItems(UsersAPI)}
      searchItemById={searchById(UsersAPI)}
      columns={columns}
      itemTitle={(user) => 'Сотрудник ' + (user?.lastName ?? '')}
      filedSet={UserFieldSet}
      canDelete={true}
      canCreate={true}
      canEdit={true}
      canView={true}
      onDelete={onDelete}
      onFinishEdit={onFinishEdit}
      onFinishCreate={onFinishCreate}
      style={ { padding: '30px' } }
    />
  );
};
