import React from 'react';
import { IUser } from '../../../../src/common_types/interfaces/User';
import { doWithResponse, UsersAPI } from '../../api';
import { formatFIO } from '../../utils';
import { AutoSuggest } from '../base/select';

type UserSelectProps = {
  isInView?: boolean;
};

export const UserSelect: React.FC<UserSelectProps> = ({ isInView = false, ...other }) => {

  const searchUsers = (term: string) => UsersAPI.get({ term }).then(res => doWithResponse(res, {
    onResult: result => result.items,
  }));

  return (
    <AutoSuggest<IUser>
      disabled={isInView}
      searchItems={searchUsers}
      getValue={user => user?.id}
      optionContent={({ item }) => (<div>{formatFIO(item)}</div>)}
      {...other}
    />
  );
};