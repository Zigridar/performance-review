import { LoginOutlined, ProfileOutlined, UserOutlined } from '@ant-design/icons';
import { Avatar, Badge, Dropdown, Menu, Typography } from 'antd';
import React from 'react';
import { useHistory } from 'react-router-dom';
import { useAuth } from '../../auth';
import { formatShortFIO } from '../../utils';
import classes from './user-content.module.less';

const { Text } = Typography;

//todo все
const PROFILE = '/profile';

export const UserContent: React.FC = () => {

  const { user, logout, loading } = useAuth();

  const history = useHistory();

  /** Перейти на страницу профиля */
  const goProfile = () => {
    const currentPath = history.location.pathname;
    if (currentPath !== PROFILE)
      history.replace(PROFILE);
  };

  const menu = (
    <Menu>
      <Menu.Item
        icon={<ProfileOutlined/>}
        onClick={goProfile}
      >
        Профиль
      </Menu.Item>
      <Menu.Item
        icon={<LoginOutlined/>}
        onClick={logout}
        disabled={loading}
      >
        Выйти
      </Menu.Item>
    </Menu>
  );

  return (
    <div className={classes.container}>
      <Text className={classes.text}>{formatShortFIO(user)}</Text>
      <Dropdown overlay={menu}>
        <Badge size={'small'} count={5}>
          <Avatar
            size='default'
            icon={<UserOutlined/>}
          />
        </Badge>
      </Dropdown>
    </div>
  );
};