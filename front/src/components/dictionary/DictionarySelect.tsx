import React from 'react';
import { INamed } from '../../../../src/common_types/interfaces/INamed';
import { CRUDType, doWithResponse } from '../../api';
import { AutoSuggest } from '../base/select';

type DictionarySelectProps<MODEL> = {
  isInView: boolean;
  crud: CRUDType<MODEL>;
};

export function DictionarySelect<MODEL extends INamed>({ isInView, crud, ...other }: DictionarySelectProps<MODEL>): JSX.Element {

  const searchItems = (term: string) => crud.get({ term }).then(res => doWithResponse(res, {
    onResult: result => result.items,
  }));

  return (
    <AutoSuggest
      disabled={isInView}
      searchItems={searchItems}
      getValue={item => item?.id}
      optionContent={({ item }) => (<>{item.name}</>)}
      {...other}
    />
  );
}