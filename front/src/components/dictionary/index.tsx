import { Form, Input, Typography } from 'antd';
import { ColumnsType } from 'antd/es/table';
import React, { useMemo } from 'react';
import { nameof } from 'ts-simple-nameof';
import { INamed } from '../../../../src/common_types/interfaces/INamed';
import { createLoadItems, CRUDType } from '../../api';
import { simpleRequired } from '../../constants/maps';
import { createEffectorModel } from '../../models/effector-model';
import { Collection, FieldSetProps } from '../base/collection';

const { Text } = Typography;

type DictionaryProps<ITEM> = {
  crud: CRUDType<ITEM>;
  itemTitle: (item: ITEM) => string;
  className?: string;
};

function DictionaryFieldSet<ITEM extends INamed>({ isInView, current }: FieldSetProps<ITEM>): JSX.Element {

  return (
    <Form.Item
      name={nameof<INamed>(n => n.name)}
      rules={[simpleRequired()]}
      initialValue={current?.name}
    >
      <Input disabled={isInView}/>
    </Form.Item>
  );
}

/** Простой справочник с текстовым поиском */
export function Dictionary<ITEM extends INamed>({ crud, itemTitle, className }: DictionaryProps<ITEM>): JSX.Element {
  
  const columns: ColumnsType<ITEM> = [
    {
      title: 'Наименование',
      key: 'name',
      render: (_, item) => {
        return (<Text>{item.name}</Text>);
      },
    },
  ];

  const { effects: {
    delete: deleteFx,
    edit: editFx,
    create: createFx,
  } } = useMemo(() => createEffectorModel<ITEM>(crud), []);

  const onDelete = (item: ITEM) => deleteFx(item);

  const onFinishCreate = (item: ITEM) => createFx(item);

  const onFinishEdit = (item: ITEM) => editFx(item);

  return (
    <Collection
      columns={columns}
      loadItems={createLoadItems<ITEM>(crud)}
      canDelete={true}
      canEdit={true}
      canCreate={true}
      canView={true}
      itemTitle={itemTitle}
      onDelete={onDelete}
      onFinishCreate={onFinishCreate}
      onFinishEdit={onFinishEdit}
      filedSet={DictionaryFieldSet}
      className={className}
      style={{ padding: '30px' }}
    />
  );
}

export { DictionarySelect } from './DictionarySelect';