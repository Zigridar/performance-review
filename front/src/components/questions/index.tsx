import { Checkbox, Form, Input, Radio, Select, Tag, Typography } from 'antd';
import { ColumnsType } from 'antd/lib/table';
import React, { useState } from 'react';
import { nameof } from 'ts-simple-nameof';
import {
  CLOSED,
  IAnswer,
  IQuestion,
  OPEN,
  QuestionType,
  questionTypes,
} from '../../../../src/common_types/interfaces/Question';
import { reviewTypes } from '../../../../src/common_types/interfaces/Review';
import { createLoadItems, QuestionsAPI, searchById } from '../../api';
import { getReviewTag, questionTypeToString, reviewTypeToString, simpleRequired } from '../../constants/maps';
import { createQuestionFx, deleteQuestionFx, editQuestionFx } from '../../models/question';
import { deepCopy } from '../../utils';
import { Collection, FieldSetProps } from '../base/collection';
import { DynamicFormList } from '../base/form-list';

const { Text } = Typography;

const questionColumns: ColumnsType<IQuestion> = [
  {
    title: 'Вопрос',
    key: nameof<IQuestion>(q => q.text),
    dataIndex: nameof<IQuestion>(q => q.text),
    width: '60%',
    render: (_, q) => {
      return (<Text>{q?.text ?? ''}</Text>);
    },
  },
  {
    title: 'Типы анкет',
    key: nameof<IQuestion>(q => q.reviewTypes),
    dataIndex: nameof<IQuestion>(q => q.reviewTypes),
    render: (_, record) => (
      <>
        {record.reviewTypes.map(getReviewTag).map((tag) => (
          <Tag key={tag.type} color={tag.color}>{tag.title}</Tag>
        ))}
      </>
    ),
  },
  {
    title: 'Тип',
    key: nameof<IQuestion>(q => q.type),
    dataIndex: nameof<IQuestion>(q => q.type),
    render: (text) => {
      return (<Text>{text === OPEN ? 'Открытый' : 'Закрытый'}</Text>);
    },
  },
];

/** Карточка вопроса */
const QuestionFieldSet: React.FC<FieldSetProps<IQuestion>> = (props) => {

  const { isInView, current: question } = props;

  const [questionType, setQuestionType] = useState<QuestionType>(question?.type ?? CLOSED);

  return (
    <>
      <Form.Item
        label={'Вопрос'}
        name={nameof<IQuestion>(q => q.text)}
        initialValue={question?.text}
        rules={[simpleRequired()]}
      >
        <Input.TextArea
          disabled={isInView}
        />
      </Form.Item>
      <Form.Item
        label={'Тип вопроса'}
        name={nameof<IQuestion>(q => q.type)}
        initialValue={question?.type ?? questionType}
        rules={[simpleRequired()]}
      >
        <Radio.Group onChange={e => setQuestionType(e.target.value)} disabled={isInView}>
          {questionTypes().map(type => (
            <Radio key={type} value={type}>
              {questionTypeToString(type)}
            </Radio>
          ))}
        </Radio.Group>
      </Form.Item>
      <Form.Item
        label={'Типы анкет'}
        name={nameof<IQuestion>(q => q.reviewTypes)}
        initialValue={question?.reviewTypes}
        rules={[simpleRequired()]}
      >
        <Select
          mode={'multiple'}
          allowClear={true}
          size={'small'}
          disabled={isInView}
        >
          {reviewTypes().map(type => (
            <Select.Option key={type} value={type}>
              {reviewTypeToString(type)}
            </Select.Option>
          ))}
        </Select>
      </Form.Item>
      <DynamicFormList
        name={nameof<IQuestion>(q => q.answers)}
        isInView={isInView}
        initialValue={question?.answers ?? []}
      >
        {
          ({ name }) => (
            <>
              <Form.Item
                label={name === 0 ? 'Ответы' : undefined}
                name={[name, nameof<IAnswer>(a => a.text)]}
                style={{ width: '80%', margin: 0 }}
                rules={[simpleRequired()]}
              >
                <Input
                  type={'text'}
                  disabled={isInView}
                />
              </Form.Item>
              {questionType === CLOSED &&
              <Form.Item
                valuePropName={'checked'}
                name={[name, nameof<IAnswer>(a => a.isCorrect)]}
                initialValue={false}
                style={{ margin: 0 }}
              >
                <Checkbox disabled={isInView}/>
              </Form.Item>
              }
            </>
          )
        }
      </DynamicFormList>
    </>
  );
};

/**
 * Реестер вопросов
 * */
export const Questions: React.FC = () => {

  /** Завершено редактирование */
  const onFinishEdit = (question: IQuestion) => {
    const copy = deepCopy(question);
    return editQuestionFx(copy);
  };

  /** Завершено создание */
  const onFinishCreate = (question: IQuestion) => {
    const copy = deepCopy(question);
    /** Для открытых - все правильные */
    if (question.type === OPEN) {
      question.answers.forEach(answer => answer.isCorrect = true);
    }
    return createQuestionFx(copy);
  };

  /** Удаление вопроса */
  const onDelete = (question: IQuestion) => {
    return deleteQuestionFx(question);
  };

  /** Валидация при сохранении вопроса */
  const validator = (question: IQuestion) => {
    /** Наличие ответов */
    if (question.answers.length < 1)
      return 'Не выбрано ответов';
    /** Если вопрос закрытый, то должен быть хотя бы один правильный */
    if (!question.answers.some(a => a.isCorrect) && question.type === CLOSED)
      return 'Нет верного ответа';
    else return null;
  };

  return (
    <Collection
      modalWidth={'700px'}
      withURLSearch
      searchItemById={searchById(QuestionsAPI)}
      columns={questionColumns}
      withURLPagination
      loadItems={createLoadItems(QuestionsAPI)}
      itemTitle={() => 'Вопрос'}
      filedSet={QuestionFieldSet}
      canDelete
      canCreate
      canEdit
      canView
      withPagination={true}
      onDelete={onDelete}
      onFinishEdit={onFinishEdit}
      onFinishCreate={onFinishCreate}
      validator={validator}
      style={{ padding: '30px' }}
    />
  );
};
