import { Layout } from 'antd';
import React, { ReactNode } from 'react';
import classes from './main-layout.module.less';

interface Props {
  /** Содержимое боковой панели */
  headerLinks: ReactNode;
  userContent: ReactNode;
  logoContent: ReactNode;
}

export const MainLayout: React.FC<Props> = ({
  headerLinks,
  children,
  userContent,
  logoContent,
}) => {

  return (
      <Layout
        className={classes.mainContainer}>
        <div className={classes.header}>
          <div className={classes.logo}>
            {logoContent}
          </div>
          <div className={classes.centerHeaderContent}>
            {headerLinks}
          </div>
          <div className={classes.user}>
            {userContent}
          </div>
        </div>
        <div className={classes.content}>
          {children}
        </div>
      </Layout>
  );
};
