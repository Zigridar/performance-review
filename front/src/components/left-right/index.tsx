import React from 'react';
import classes from './left-right.module.less';

type PanelProps = {
  left: React.ReactNode;
  right: React.ReactNode;
};

export const LeftRightPanel: React.FC<PanelProps> = (props) =>{

  const { left, right } = props;

  return (
    <div className={classes.container}>
      <div className={classes.item}>
        {left}
      </div>
      <div className={classes.item}>
        {right}
      </div>
    </div>
  );
};