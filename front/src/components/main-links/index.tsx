import React, { ReactNode } from 'react';
import { Link, useLocation } from 'react-router-dom';
import classes from './main-links.module.scss';

interface MainLinksProps {
  menuItems: IMenuItem[];
}

export interface IMenuItem {
  path: string;
  menuContent: ReactNode;
  content: ReactNode;
}

export const MainLinks: React.FC<MainLinksProps> = (props) => {

  const { menuItems } = props;

  const location = useLocation();

  return (
    <div className={classes.container}>
      {menuItems.map(item => (
        <Link
          className={classes.link}
          key={item.path}
          to={item.path}
          style={item.path === location.pathname ? { color: 'black' } : null}
        >
          {item.menuContent}
        </Link>
      ))}
    </div>
  );
};