import { Tabs } from 'antd';
import React from 'react';
import { INamed } from '../../../../src/common_types/interfaces/INamed';
import { CRUDType, PostAPI, ProjectAPI, TeamAPI } from '../../api';
import { Dictionary } from '../dictionary';
import { Users } from '../users';
import classes from './admin.module.less';

const { TabPane } = Tabs;

type TabContentWrapperProps<MODEL> = {
  crud: CRUDType<MODEL>;
  itemTitle: (item: MODEL) => string;
};

function TabContentWrapper<ITEM extends INamed>({ crud, itemTitle }: TabContentWrapperProps<ITEM>): JSX.Element {

  return (
    <Dictionary
      crud={crud}
      itemTitle={itemTitle}
    />
  );
}

export const AdminPage: React.FC = () => {

  const tabs = [
    {
      content: <TabContentWrapper
        crud={TeamAPI}
        itemTitle={() => 'Команды'}
      />,
      title: 'Команды',
    },
    {
      content: <TabContentWrapper
        crud={PostAPI}
        itemTitle={() => 'Должность'}
      />,
      title: 'Должности',
    },
    {
      content: <TabContentWrapper
        crud={ProjectAPI}
        itemTitle={() => 'Проект'}
      />,
      title: 'Проекты',
    },
  ];

  return (
    <Tabs
      className={classes.tabContainer}
      tabPosition='left'
    >
      {tabs.map(({ title, content }) => (
        <TabPane
          tab={title}
          key={title}
        >
          {content}
        </TabPane>
      ))}
      <TabPane
        tab='Сотрудники'
        key='employees'
      >
        <Users/>
      </TabPane>
    </Tabs>
  );
};