import { CheckOutlined, EditOutlined, MoreOutlined, RollbackOutlined, StopOutlined } from '@ant-design/icons';
import { Button, Form, FormInstance, Input, Modal, Popconfirm, Popover, Space, Table, Typography } from 'antd';
import { ColumnsType } from 'antd/lib/table';
import { createEvent, Event } from 'effector';
import React, { CSSProperties, ReactNode, useEffect, useMemo, useState } from 'react';
import { NumberParam, StringParam, useQueryParam } from 'use-query-params';
import { GetResponse, IWithId, QueryFilter } from '../../../../../src/common_types/API_types';
import classes from './collection.module.less';

const { Text } = Typography;
const { useForm } = Form;

/** Состояния формы */
const VIEW = 'VIEW';
const EDIT = 'EDIT';
const CREATE = 'CREATE';

type FormMode = typeof VIEW | typeof EDIT | typeof CREATE;

/** Событие сохранения */
const OK = 'OK';

/** Событие отмены */
const CANCEL = 'CANCEL';

type FormEvent = typeof OK | typeof CANCEL;

/**
 * Свойства формы
 * */
interface FormProps<ITEM extends IWithId> {
  /** Поля формы */
  children: (form: FormInstance<ITEM>) => ReactNode;
  /** Шина событий */
  event: Event<FormEvent>;
  /** Сохранение формы */
  onSuccess: (item: ITEM) => void;
  /** Валидатор */
  validator?: (item: ITEM) => string | null;
  /** Возможно ли редактирование */
  canEdit: boolean;
}

/**
 * Форма просмотра/редактирования/создания
 * */
function ItemForm<ITEM extends IWithId>(props: FormProps<ITEM>): JSX.Element {

  const {
    onSuccess,
    children,
    validator = () => null,
    event,
  } = props;

  const [form] = useForm<ITEM>();

  useEffect(() => {
    /** Попытка сохранить форму */
    const onOk = () => {
      form.validateFields()
        .then(() => {
          const item = form.getFieldValue(undefined) as ITEM;
          const validRes = validator(item);
          if (!validRes) {
            onSuccess(item);
            form.resetFields();
          } else {
            Modal.error({
              content: <Text>{validRes}</Text>,
            });
          }
        }).catch((e) => {});
    };

    /** Отмена формы */
    const cancel = () => {
      form.resetFields();
    };

    const subscription = event.watch((formEvent) => {
      switch (formEvent) {
        case CANCEL:
          cancel();
          break;
        case OK:
          onOk();
          break;
      }
    });

    return () => subscription.unsubscribe();
  }, []);

  return (
    <Form
      form={form}
      layout={'vertical'}
    >
      {children(form)}
    </Form>
  );
}

/** Для табличного маппинга */
type ItemWithKeyAndNumber<ITEM> = ITEM & {
  key: string;
  number: number;
};

/** Обязательные пропсы карточки */
interface IFieldSetProps<ITEM extends IWithId> {
  current: ITEM;
  isInView: boolean;
  form: FormInstance<ITEM>;
}

/** Тип параметров карточки элемента */
export type FieldSetProps<ITEM extends IWithId, PROPS extends object = object> = IFieldSetProps<ITEM> & PROPS;

/**
 * Свойства коллекции
 * */
interface CollectionProps<ITEM extends IWithId, CARD extends object, FILTERS extends object = object> {
  /** Выборка, если известна зарвнее */
  items?: ITEM[];
  useOnlyItems?: boolean;
  /** Асинхронная загрузка элементов коллекции с учетом фильтра */
  loadItems?: (filter: QueryFilter<FILTERS>) => Promise<GetResponse<ITEM>>
  /** Колонки */
  columns: ColumnsType<ITEM>;
  /** Ширина модального окна */
  modalWidth?: string | number;
  /** Поля формы */
  filedSet?: React.FC<FieldSetProps<ITEM, CARD>>;
  /** Дополнительные параметры формы */
  fieldSetProps?: CARD;
  /** Подпись при создании/редактировании/просмотре */
  itemTitle?: (item: ITEM) => string;
  /** Возможность создания */
  canCreate?: boolean;
  /** Возможность редактирования */
  canEdit?: boolean;
  /** Возможность просмотра */
  canView?: boolean;
  /** Возможность удаления */
  canDelete?: boolean;
  /** Вызывается при завершении создания */
  onFinishCreate?: (item: ITEM) => Promise<ITEM>;
  /** Вызывается при завершении редактирования */
  onFinishEdit?: (item: ITEM) => Promise<ITEM>;
  /** Вызывается при удалении */
  onDelete?: (item: ITEM) => Promise<ITEM>;
  /** Открытие карточки */
  onCardOpen?: (item: ITEM) => void;
  /** Отмена */
  onCancel?: () => void;
  /** Валидатор */
  validator?: (item: ITEM) => string | null;
  /** Асинхронный поиск элемента по id */
  searchItemById?: (id: string) => Promise<ITEM>;
  /** Осуществлять ли поиск по строке URL */
  withURLSearch?: boolean;
  /** Пагинация по URL */
  withURLPagination?: boolean;
  /** Стили контйенера */
  style?: CSSProperties;
  /** Подпись поиска */
  searchPlaceHolder?: string;
  /** Фильтры */
  filters?: FILTERS;
  /** Элементы тулбара */
  toolbarNodes?: ReactNode | Array<ReactNode>;
  /** Текстовый поиск */
  withTextSearch?: boolean;
  /** Класс таблицы */
  tableClassName?: string;
  /** Класс контейнера */
  className?: string;
  /** Прелбразование элементов */
  mapItem?: (item: ITEM) => ITEM;
  /** Использовать пагинацию */
  withPagination?: boolean;
  /** Использование внешнего триггера на создание */
  externalCreate?: {
    isInCreate: boolean;
  };
}

/** Маппинг элементов */
function mapDataToTableItem<ITEM extends IWithId>(items: Array<ITEM>): ItemWithKeyAndNumber<ITEM>[] {
  return items.map((item, index) => ({
    ...item,
    key: item.id,
    number: index + 1,
  }));
}

/**
 * Коллекция с различными возможностями
 * */
export function Collection<ITEM extends IWithId, CARD extends object, FILTERS extends object>(props: React.PropsWithChildren<CollectionProps<ITEM, CARD>>): JSX.Element {

  const {
    items = [],
    loadItems = (filter: QueryFilter<FILTERS>) => Promise.resolve({ items, count: items.length }),
    searchItemById = (id) => loadItems({ id }).then(res => res.items[0]),
    withURLSearch = false,
    withURLPagination = false,
    columns,
    canCreate = false,
    canEdit = false,
    canView = false,
    canDelete = false,
    itemTitle = item => item?.toString(),
    onDelete,
    onFinishCreate,
    onFinishEdit,
    onCancel,
    filedSet: FieldSet,
    fieldSetProps = {} as CARD,
    onCardOpen = (item: ITEM) => {},
    validator = () => null,
    style = {},
    searchPlaceHolder = 'Поиск',
    filters,
    toolbarNodes,
    modalWidth,
    withTextSearch = true,
    tableClassName = classes.table,
    className,
    useOnlyItems = false,
    mapItem = (item: ITEM) => item,
    withPagination = true,
    externalCreate,
  } = props;

  /** Загрузка при операциях редактирования */
  const [loading, setLoading] = useState<boolean>(false);

  /** Режим создания */
  const [isInCreate, setIsInCreate] = useState(false);

  const isInCreateWithExternal = isInCreate || externalCreate && externalCreate.isInCreate;

  /** Режим редактирования */
  const [isInEdit, setIsInEdit] = useState(false);

  /** Режим просмотра */
  const [isInView, setIsInView] = useState(false);

  /** Текущий жлемент */
  const [current, setCurrent] = useState<ITEM>(null);

  /** id элемента коллекции в URL строке */
  const [idFromURL, setIdToURL] = useQueryParam('id', StringParam);

  /** Текущяая страница */
  const [currentPageFromURL, setCurrentPageToURL] = useQueryParam('currentPage', NumberParam);

  /** Количество элементов отображаемых на странице */
  const [pageSizeFromURL, setPageSizeToURL] = useQueryParam('pageSize', NumberParam);

  /** Условие полнотекстового поиска */
  const [termFromURL, setTermToUrl] = useQueryParam('term', StringParam);

  /** Сохранение параметров URL при открытии карточки элемента */
  const [storedFilter, setStoredFilter] = useState<QueryFilter>({ pageSize: 5, term: undefined, currentPage: 1 });

  /** Количество элементов, удовлетворяющих запросу */
  const [totalCount, setTotalCount] = useState<number>(0);

  /** Мануальное обновление */
  const [changed, setChanged] = useState<number>(0);

  /** Изменить состояние */
  const change = () => setChanged(prev => prev + 1);

  /** true - если карточка элемента не открыта */
  const noIsInItem = !current && !isInCreateWithExternal && !isInView && !isInEdit;

  /** Удаление элемента */
  const deleteItem = (item: ITEM) => {
    setLoading(true);
    onDelete(item)
      .finally(() => {
        setLoading(false);
        change();
      });
  };

  /** Создание элемента */
  const finishCreateItem = (item: ITEM) => {
    setLoading(true);
    onFinishCreate(item)
      .finally(() => setLoading(false));
  };

  /** Редактирование элемента */
  const finishEditItem = (item: ITEM) => {
    setLoading(true);
    onFinishEdit(item)
      .finally(() => setLoading(false));
  };

  /** Текущее отображение карточки */
  const currentMode: () => FormMode = () => {
    if (isInEdit)
      return EDIT;
    if (isInCreateWithExternal)
      return CREATE;
    if (isInView)
      return VIEW;
  };

  /** Отмена редактирования или создания */
  const cancel = () => {
    if (isInCreateWithExternal)
      setIsInCreate(false);
    else if (isInEdit)
      setIsInEdit(false);
    else if (isInView)
      setIsInView(false);

    if (withURLSearch && !!idFromURL) {
      setIdToURL(undefined, 'replaceIn');
    }

    setCurrent(null);
    if (onCancel)
      onCancel();
  };

  /** Сохранение при редактировании или создании */
  const onSuccess = (item: ITEM) => {
    if (isInEdit) {
      finishEditItem({
        ...item,
        id: current.id,
      });
    } else if (isInCreateWithExternal) {
      finishCreateItem(item);
    }
    cancel();
  };

  /** Начало редактирования */
  const beginEdit = (item: ITEM) => {
    onCardOpen(item);
    setCurrent(item);
    if (isInView)
      setIsInView(false);
    setIsInEdit(true);
    if (withURLPagination && noIsInItem) {
      setStoredFilter({
        currentPage: currentPageFromURL,
        term: termFromURL,
        pageSize: pageSizeFromURL,
      });
    }
    if (withURLSearch && !idFromURL) {
      setIdToURL(item.id, 'replace');
    }
  };

  /** Начало просмотра */
  const beginView = (item: ITEM) => {
    onCardOpen(item);
    setCurrent(item);
    setIsInView(true);
    if (withURLPagination && noIsInItem) {
      setStoredFilter({
        currentPage: currentPageFromURL,
        term: termFromURL,
        pageSize: pageSizeFromURL,
      });
    }
    if (withURLSearch) {
      setIdToURL(item.id, 'replace');
    }
  };

  /** Назад */
  const onBack = () => {
    setCurrent(null);
    setIsInView(false);
    if (withURLSearch && idFromURL) {
      setIdToURL(undefined, 'replaceIn');
    }
    if (withURLPagination) {
      const { pageSize, term, currentPage } = storedFilter;
      if (pageSize)
        setPageSizeToURL(pageSize, 'replaceIn');
      if (term)
        setTermToUrl(term, 'replaceIn');
      if (currentPage)
        setCurrentPageToURL(currentPage, 'replaceIn');
    }
  };

  /** Коллекция для отображения в таблице */
  const [dataSource, setDataSource] = useState<Array<ItemWithKeyAndNumber<ITEM>>>([]);

  /** Использование только переданных значений */
  useEffect(() => {
    if (useOnlyItems)
      setDataSource(mapDataToTableItem(items.map(mapItem)));
  }, [items]);

  /** Синхронизация с URL, если установлен withURLSearch */
  useEffect(() => {
    if (withURLSearch && !!idFromURL && noIsInItem) {
      /** Если элемент установлен - ищем */
      setLoading(true);
      searchItemById(idFromURL)
        .then(item => {
          /** Элемент найден - открываем карточку */
          if (!!item) {
            beginView(item);
          /** Иначе очищаем строку */
          } else {
            setIdToURL(undefined, 'replaceIn');
          }
        })
        .finally(() => setLoading(false));
    } else if (noIsInItem) {
      onBack();
    }
  }, [idFromURL]);

  /** Синхронизация с URL при пагинации */
  useEffect(() => {

    const { currentPage: storedCurrentPage = 1, term: storedTerm, pageSize: storedPageSize = 5 } = storedFilter;

    if (noIsInItem && withURLPagination && (!currentPageFromURL || !pageSizeFromURL)) {
      if (!currentPageFromURL) {
        setCurrentPageToURL(storedCurrentPage, 'replaceIn');
      }
      if (!pageSizeFromURL) {
        setPageSizeToURL(storedPageSize, 'replaceIn');
      }
      if (!termFromURL) {
        setTermToUrl(storedTerm, 'replaceIn');
      }
      if (!storedFilter.pageSize || !storedFilter.currentPage) {
        setStoredFilter(prev => ({
          ...prev,
          currentPage: storedCurrentPage,
          pageSize: storedPageSize,
        }));
      }
    }

    /** Загрузка элементов с учетом пагинации и поиска */
    if (noIsInItem && withURLPagination && currentPageFromURL && pageSizeFromURL) {
      setLoading(true);
      const filter: QueryFilter = { currentPage: storedCurrentPage, pageSize: storedPageSize };
      /** Добавление полнотекстового поиска */
      if (storedTerm)
        filter.term = storedTerm;
      /** Добавление внешних фильтров */
      if (filters)
        filter.additionalFilters = filters;

      /** Обновление коллекции */
      loadItems(filter)
        .then(({ items, count = items.length }) => {
          /** Установка кол-ва элементов */
          setTotalCount(count);
          const data = mapDataToTableItem(items.map(mapItem));
          setDataSource(data);
        })
        .finally(() => {
          setLoading(false);
        })
        .catch(e => {
          console.error(e);
        });
    }
  }, [currentPageFromURL, pageSizeFromURL, termFromURL, noIsInItem]);

  /** Если коллекция простая, то установить выборку */
  useEffect(() => {
    if (!withURLPagination && !withURLSearch) {

      const { term, currentPage, pageSize } = storedFilter;

      setLoading(true);
      loadItems({ term, currentPage, pageSize })
        .then(({ items, count }) => {
          const data = mapDataToTableItem(items.map(mapItem));
          setDataSource(data);
          setTotalCount(count);
        })
        .finally(() => setLoading(false));
    }
  }, [storedFilter, noIsInItem, changed]);

  /** Изменение пагинации */
  const onChangePagination = (newPageNumber: number, newPageSize: number) => {
    setStoredFilter(prev => ({
      ...prev,
      pageSize: newPageSize,
      currentPage: newPageNumber,
    }));

    if (withURLPagination && newPageNumber !== currentPageFromURL) {
      setCurrentPageToURL(newPageNumber, 'replaceIn');
    }
    if (withURLPagination && newPageSize != pageSizeFromURL) {
      setPageSizeToURL(newPageSize, 'replaceIn');
    }
  };

  /** Текстовый поиск */
  const onTextSearch = (text: string) => {
    setStoredFilter(prev => ({
      ...prev,
      term: text,
      currentPage: 1,
    }));

    if (withURLSearch) {
      if (text && text !== termFromURL) {
        setTermToUrl(text, 'replaceIn');
      } else if (!text) {
        setTermToUrl(undefined, 'replaceIn');
      }
    }
  };

  /** Колонка номера */
  const numberColumn = {
    dataIndex: 'number',
    key: 'number',
    width: '5%',
    title: '№',
  };

  /** Добавление колонки с номером */
  const columnsWithNumber: ColumnsType<ITEM> = [
    numberColumn,
    ...columns,
  ];

  /** Если есть возможность редактировать или удалять - добавляем действия */
  if (canDelete || canEdit || canView)
    columnsWithNumber.push({
      key: 'actions',
      width: '5%',
      render: (record: ItemWithKeyAndNumber<ITEM>) => {
        /** Возможные действия с элементом коллекции */
        const content = (
          <Space direction={'vertical'} size={'small'}>
            {canView &&
              <Button
                type='link'
                onClick={() => beginView(record)}
              >
                Просмотр
              </Button>
            }
            {canEdit &&
            <Button
              type='link'
              onClick={() => beginEdit(record)}
            >
              Редактировать
            </Button>
            }
            {canDelete &&
              <Popconfirm
                title={'Удалить?'}
                onConfirm={() => deleteItem(record)}
              >
                <Button
                  type='link'
                >
                  Удалить
                </Button>
              </Popconfirm>
            }
          </Space>
        );

        return (
          <Popover
            visible={noIsInItem ? undefined : false}
            trigger='click'
            content={content}
          >
            <MoreOutlined/>
          </Popover>
        );
      },
    });

  /** Шина событий для формы */
  const formEvent = useMemo(() => createEvent<FormEvent>(), []);

  /** сохранение в форме */
  const onFormOk = () => {
    formEvent(OK);
  };

  /** Отмена в форме */
  const onFormCancel = () => {
    formEvent(CANCEL);
    cancel();
  };

  /** Кнопки формы */
  const footer = (
    <>
      {(currentMode() === EDIT || currentMode() === CREATE) &&
      <Space size={'small'}>
        <Button
          onClick={onFormCancel}
          className={classes.borderRadius}
          icon={<StopOutlined/>}
        >
          Отмена
        </Button>
        <Button
          onClick={onFormOk}
          className={classes.borderRadius}
          type={'primary'}
          icon={<CheckOutlined/>}
        >
          Сохранить
        </Button>
      </Space>
      }
      {(currentMode() === VIEW) &&
      <Space size={'small'}>
        {beginEdit && canEdit &&
        <Button
          onClick={() => beginEdit(current)}
          className={classes.borderRadius}
          type={'primary'}
          icon={<EditOutlined/>}
        >
          Редактировать
        </Button>
        }
        <Button
          onClick={onBack}
          className={classes.borderRadius}
          type={'primary'}
          icon={<RollbackOutlined/>}
        >
          Назад
        </Button>
      </Space>
      }
    </>
  );

  return (
    <div className={className} style={style}>
      <div className={classes.toolbar}>
        {withTextSearch &&
          <Input.Search
            className={classes.textSearch}
            placeholder={searchPlaceHolder}
            onSearch={onTextSearch}
          />
        }
        {toolbarNodes}
        {!externalCreate && canCreate &&
          <Button
            disabled={loading}
            className={classes.createButton}
            onClick={() => setIsInCreate(true)}
          >
            Добавить
          </Button>
        }
      </div>
      <Table
        loading={loading}
        dataSource={dataSource}
        columns={columnsWithNumber}
        className={tableClassName}
        pagination={ withPagination ? {
          pageSize: withURLPagination ? pageSizeFromURL : storedFilter.pageSize,
          current: withURLPagination ? currentPageFromURL : storedFilter.currentPage,
          total: totalCount,
          showSizeChanger: false,
          size: 'small',
          onChange: onChangePagination,
        } : false }
      />
      {FieldSet &&
        <Modal
          visible={isInCreateWithExternal || isInEdit || isInView}
          closable={false}
          destroyOnClose={true}
          onCancel={cancel}
          title={<Text>{itemTitle(current)}</Text>}
          footer={footer}
          centered
          forceRender={false}
          width={modalWidth}
          bodyStyle={{
            maxHeight: '70vh',
            overflowY: 'auto',
          }}
        >
          <ItemForm
            event={formEvent}
            canEdit={canEdit}
            onSuccess={onSuccess}
            validator={validator}
          >
            {(form) => <FieldSet form={form} {...fieldSetProps} current={current} isInView={loading || currentMode() === VIEW}/>}
          </ItemForm>
        </Modal>
      }
    </div>
  );
}