import { DeleteOutlined, PlusOutlined } from '@ant-design/icons';
import { Button, Form, Space } from 'antd';
import { FormListFieldData } from 'antd/es/form/FormList';
import React from 'react';
import classes from './form-list.module.less';

type FormListProps = {
  name: string;
  isInView?: boolean;
  addText?: string;
  initialValue?: Array<any>;
  children: (data: FormListFieldData) => React.ReactNode;
};

/** Динамическая коллекция в форме */
export const DynamicFormList: React.FC<FormListProps> = (props) => {

  const {
    name,
    isInView = false,
    initialValue,
    addText = 'Добавить',
    children,
    ...rest
  } = props;

  return (
    <Form.List
      name={name}
      initialValue={initialValue}
      {...rest}
    >
      {
        (fields, { add, remove }) => {

          const itemFields = fields.map((item) => {
            return (
              <div
                key={item.key}
                className={classes.fieldsContainer}
              >
                {children(item)}
                {!isInView &&
                <DeleteOutlined
                  disabled={isInView}
                  onClick={() => remove(item.name)}
                />
                }
              </div>
            );
          });

          return (
            <Space
              direction={'vertical'}
              className={classes.fullWidth}
            >
              {itemFields}
              {!isInView &&
              <Form.Item>
                <Button
                  type={'dashed'}
                  onClick={() => add()}
                  block
                  icon={<PlusOutlined/>}
                >
                  {addText}
                </Button>
              </Form.Item>
              }
            </Space>
          );
        }
      }
    </Form.List>
  );
};