import { Select, Spin } from 'antd';
import React, { CSSProperties, useEffect, useMemo, useState } from 'react';
import Timer = NodeJS.Timer;

type MultiSelectProps<ITEM> = {
  /** async load items by text term */
  searchItems: (text: string) => Promise<Array<ITEM>>;
  /** get uniq identifier of item */
  getItemIdentifier: (item: ITEM) => string;
  /** get item text label */
  getItemLabel?: (item: ITEM) => string;
  /** change selected items */
  onChange: (items: Array<ITEM>) => void;
  /** catch error in search */
  onSearchError?: (error: any) => void;
  /** initial selected items */
  initialValue?: Array<ITEM>;
  /** if it`s set to true then the same text request will not be sent */
  useSearchCache?: boolean;
  /** send request after last key up event */
  keyUpDelay?: number;
  /** CSS style properties */
  style?: CSSProperties;
};

type Option = {
  label: string;
  value: string;
};

/** default delay */
const KEY_UP_DELAY = 500;

const CentredSpinner: React.FC = () => {
  return (
    <div
      style={{
        display: 'flex',
        justifyContent: 'space-evenly',
        width: '100%',
      }}
    >
      <Spin size={'small'}/>
    </div>
  );
};

export function MultiSelect<ITEM>(props: MultiSelectProps<ITEM>): JSX.Element {

  const {
    initialValue = [],
    searchItems,
    getItemIdentifier,
    onChange: onExternalChange,
    getItemLabel = (item: ITEM) => `${item}`,
    onSearchError = (error: any) => {},
    useSearchCache = true,
    keyUpDelay = KEY_UP_DELAY,
    style = {},
  } = props;

  /** loading state */
  const [loading, setLoading] = useState<boolean>(false);

  /** search text term */
  const [searchText, setSearchText] = useState<string>('');

  /** items for select */
  const [items, setItems] = useState<Array<ITEM>>(initialValue);

  /** selected item ids */
  const [selectedIds, setSelectedIds] = useState<Set<string>>(new Set(initialValue.map(getItemIdentifier)));

  /** restrict timer */
  const [requestTimer, setRequestTimer] = useState<Timer>(setTimeout(() => {}, 0));

  /** can send request */
  const canRequest = !requestTimer;

  /** map index of items */
  const itemIndex = useMemo(() => {
    const index = new Map<string, ITEM>();
    items.forEach(item => {
      index.set(getItemIdentifier(item), item);
    });
    return index;
  }, []);

  /** search cache */
  const searchCache = useMemo(() => new Map<string, Array<ITEM>>(), []);

  /** clear selected */
  const onClear = () => {
    setSelectedIds(new Set());
  };

  /** change selected value */
  const onChange = (values: Array<string>) => {
    setSelectedIds(new Set(values));
    setSearchText('');
    const selected = values.map(value => itemIndex.get(value));
    onExternalChange(selected);
  };

  /** search items */
  const onSearch = (text: string) => {
    setSearchText(text);
  };

  /** blur event */
  const onBlur = () => setSearchText('');

  /** block requests */
  const onKeyUp = () => {
    clearTimeout(requestTimer);
    const timer = setTimeout(() => {
      setRequestTimer(null);
    }, keyUpDelay);
    setRequestTimer(timer);
  };

  /** synchronize */
  useEffect(() => {
    if (canRequest) {
      /** set states */
      setLoading(true);
      setItems([]);

      /** select promise */
      const searchPromise = useSearchCache && searchCache.has(searchText) ?
        Promise.resolve(searchCache.get(searchText)) :
        searchItems(searchText);

      searchPromise
        .then(res => {
          res.forEach(item => itemIndex.set(getItemIdentifier(item), item));
          /** save cache if needed */
          if (useSearchCache && !searchCache.has(searchText))
            searchCache.set(searchText, res);

          setSearchText(prevText => {
            /** check actual request */
            if (searchText === prevText && items !== res) {
              setItems(res);
              setLoading(false);
            }
            return prevText;
          });
        })
        .catch(onSearchError);
    }
  }, [searchText, canRequest]);

  /** make options */
  const options: Array<Option> = items.map(item=>({
    value: getItemIdentifier(item),
    label: getItemLabel(item),
  }));

  return (
    <Select
      allowClear
      showSearch
      onKeyUp={onKeyUp}
      onBlur={onBlur}
      showArrow={!loading}
      filterOption={false}
      value={Array.from(selectedIds)}
      mode={'multiple'}
      onClear={onClear}
      onChange={onChange}
      notFoundContent={loading ? <CentredSpinner/> : undefined}
      loading={loading}
      onSearch={onSearch}
      style={{ width: '100%', ...style }}
      options={options}
    />
  );
}