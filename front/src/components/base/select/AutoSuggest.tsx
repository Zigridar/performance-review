import { Select, Spin } from 'antd';
import React, { CSSProperties, useEffect, useMemo, useState } from 'react';
import { IWithId } from '../../../../../src/common_types/API_types';

const { Option } = Select;

/** Обязательные пропсы элемента списка */
interface IOptionProps<ITEM> {
  item: ITEM;
}

/** Тип параметров опции элемента */
export type OptionProps<ITEM extends IWithId, PROPS extends object = object> = IOptionProps<ITEM> & PROPS;

/**
 * Свойства AutoSuggest
 * */
interface AutoSuggestProps<ITEM extends IWithId, OPTION extends object> {
  searchItems: (text: string, id?: string) => Promise<Array<ITEM>>;
  onChange?: (item: ITEM) => void;
  getValue: (item: ITEM) => string;
  optionContent: React.FC<OptionProps<ITEM, OPTION>>;
  disabled?: boolean;
  useSearchCache?: boolean;
  value?: ITEM;
  optionProps?: OPTION;
  style?: CSSProperties;
}

/**
 * Элемент для поиска и выбора значений
 * */
export function AutoSuggest<ITEM extends IWithId, OPTION extends object = object>(props: React.PropsWithChildren<AutoSuggestProps<ITEM, OPTION>>): JSX.Element {

  const {
    searchItems,
    optionContent: OptionContent,
    optionProps = {} as OPTION,
    style = {},
    getValue,
    disabled = false,
    onChange: onSelectItem = (item: ITEM) => {},
    useSearchCache = true,
    value,
    ...other
  } = props;

  /** Статус загрузки */
  const [loading, setLoading] = useState<boolean>(false);

  /** Текст для поиска */
  const [searchText, setText] = useState<string>('');

  /** Элементы для выбора */
  const [items, setItems] = useState<Array<ITEM>>(value ? [value] : []);

  /** Индекс элементов текущего поиска */
  const itemIndex = useMemo(() => {
    const index = new Map<string, ITEM>();
    items.forEach(item => {
      index.set(item.id, item);
    });
    return index;
  }, [items, value]);

  useEffect(() => {
    if (value) {
      searchItems('', getValue(value))
        .then(res => {
          if (res.length)
            setItems(res);
        });
    }
  }, [value]);

  /** Кэш поиска */
  const searchCache = useMemo(() => new Map<string, Array<ITEM>>(), []);

  /** Выбор элемента */
  const onSelect = (id: string) => {
    const item = itemIndex.get(id);
    onSelectItem(item);
  };

  /** Поиск элементов */
  const onSearch = (text: string) => {
    setText(text);
  };

  /** Очистка выбора */
  const onClear = () => {
    setText('');
    onSelectItem(null);
  };

  /** Осуществить поиск, если изменился текст поиска */
  useEffect(() => {
    /** Установка загрузки */
    setLoading(true);
    /** Сброс элементов */
    setItems([]);

    /** Если есть в кэше, то достоть из него, иначе искать */
    const itemsPromise = useSearchCache && searchCache.has(searchText) ? Promise.resolve(searchCache.get(searchText)) : searchItems(searchText);

    itemsPromise
      .then(items => {
        if (useSearchCache)
          searchCache.set(searchText, items);
        setItems(items);
      })
      .catch(e => {
        //todo
        console.error(e);
      })
      .finally(() => setLoading(false));
  }, [searchText]);

  return (
    <Select<string>
      loading={loading}
      labelInValue={false}
      disabled={disabled}
      onSelect={onSelect}
      onSearch={onSearch}
      onClear={onClear}
      filterOption={false}
      notFoundContent={loading ? <Spin size={'small'} /> : undefined}
      showSearch
      allowClear
      style={{ width: '100%', ...style }}
      value={getValue(value)}
      {...other}
    >
      {items.map((item) => {
        return (
          <Option key={item.id} value={item.id}>
            <OptionContent item={item} {...optionProps}/>
          </Option>
        );
      })}
    </Select>
  );
}