import axios, { AxiosResponse } from 'axios';
import {
  AuthBody,
  FormAdditionFilters,
  GetResponse,
  PRAdditionalFilter,
  QueryFilter,
  ResType,
} from '../../../src/common_types/API_types';
import { IForm } from '../../../src/common_types/interfaces/Form';
import { INamed } from '../../../src/common_types/interfaces/INamed';
import { IPerformanceReview } from '../../../src/common_types/interfaces/PerformanceReview';
import { IQuestion } from '../../../src/common_types/interfaces/Question';
import { IUser } from '../../../src/common_types/interfaces/User';

/** Пути для запросов */
const BASE_URL = 'api';
const QUESTIONS_PATH = 'questions';
const FORMS_PATH = 'forms';
const USERS_PATH = 'users';
const PR_PATH = 'prs';
const AUTH = 'auth';
const POST = 'post';
const TEAM = 'team';
const PROJECT = 'project';

/** Таймаут запроса */
const REQUEST_TIMEOUT = 60 * 1000;

export const apiInstance = axios.create({
  timeout: REQUEST_TIMEOUT,
  baseURL: BASE_URL,
});

/**
 * Модель простого API на создание, чтение, удаление и обновление данных
 * */
export type CRUDType<MODEL, FILTERS = object> = {
  /** Получение данных модели с фильтрацией */
  get: (filter?: QueryFilter<FILTERS>) => Promise<ResType<GetResponse<MODEL>>>;
  /** Обновление */
  update: (model: MODEL) => Promise<ResType<void>>;
  /** Создание */
  create: (model: MODEL) => Promise<ResType<MODEL>>;
  /** Удаление */
  delete: (model: MODEL) => Promise<ResType<void>>;
}; 

/**
 * API авторизации
 * */
type AuthAPIType = {
  /** Авторизация */
  login: (credentials: AuthBody) => Promise<ResType<IUser>>;
  /** Выход */
  logout: () => Promise<ResType<void>>;
  /** Получение текущего пользователя по cookie */
  getCurrentUser: () => Promise<ResType<IUser>>;
};


/** Создание API под модель
 * @param path - относительный путь для запроса
 * */
const createAPI = <MODEL, FILTER = object>(path: string) => ({
  get(filter: QueryFilter<FILTER> = {}) {
    return apiInstance.get<ResType<GetResponse<MODEL>>>(path, { params: filter })
      .then(res => res.data);
  },
  create(model: MODEL) {
    return apiInstance.post<MODEL, AxiosResponse<ResType<MODEL>>>(path, model)
      .then(res => res.data);
  },
  update(model: MODEL) {
    return apiInstance.put<ResType<void>, AxiosResponse<ResType<void>>, MODEL>(path, model)
      .then(res => res.data);
  },
  delete(model: MODEL) {
    return apiInstance.delete<ResType<void>, AxiosResponse<ResType<void>>, MODEL>(path, { data: model })
      .then(res => res.data);
  },
});

/** api авторизации */
export const AuthAPI: AuthAPIType = {
  login: (credentials) => {
    return apiInstance.post<void, AxiosResponse<ResType<IUser>>, AuthBody>(AUTH, credentials)
      .then(res => res.data);
  },
  logout: () => {
    return apiInstance.delete<void, AxiosResponse<ResType<void>>>(AUTH)
      .then(res=> res.data);
  },
  getCurrentUser: () => {
    return apiInstance.post<void, AxiosResponse<ResType<IUser>>>(AUTH + '/auto')
      .then(res => res.data);
  },
};

/** API для вопросов */
export const QuestionsAPI = createAPI<IQuestion>(QUESTIONS_PATH);
/** API для анкет */
export const FormsAPI = createAPI<IForm, FormAdditionFilters>(FORMS_PATH);
/** API для PR */
export const PrAPI = createAPI<IPerformanceReview, PRAdditionalFilter>(PR_PATH);
/** API для пользователей */
export const UsersAPI = createAPI<IUser>(USERS_PATH);
/** API для должностей */
export const PostAPI = createAPI<INamed>(POST);
/** API для команд */
export const TeamAPI = createAPI<INamed>(TEAM);
/** API для проектов */
export const ProjectAPI = createAPI<INamed>(PROJECT);


type ResponseHandlers<T, E, R> = {
  onError?: (message: string) => E;
  onResult: (result: T) => R;
};

/** Обработка ответа от сервера */
export const doWithResponse: <T, E, R>(response: ResType<T>, handlers: ResponseHandlers<T, E, R>) => R | E =
  <T, E, R>(response: ResType<T>, handlers: ResponseHandlers<T, E, R>) => {

    const {
      onResult,
      onError = (message: string) => {
        throw new Error(message);
      },
    } = handlers;

    switch (response.type) {
      case 'Result':
        return onResult(response.data);
      case 'Error':
        return onError(response.message);
    }
  };

/** Поиск по id */
export const searchById = <MODEL>(crud: CRUDType<MODEL>) => {
  return (id: string) => crud.get({ id })
    .then(response => {
      return doWithResponse<GetResponse<MODEL>, MODEL, MODEL>(response, {
        onResult: (res) => res.items[0],
        onError: () => null,
      });
    });
};

/** Создание функции загрузки элементов по фильтру */
export const createLoadItems = <MODEL, FILTER = object>(api: CRUDType<MODEL, FILTER>) => (filter: QueryFilter<FILTER>) => {
  return api.get(filter)
    .then(res => {
      return doWithResponse<GetResponse<MODEL>, never, GetResponse<MODEL>>(res, {
        onResult: result => result,
      });
    });
};