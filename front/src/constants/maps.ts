import { CLOSED, OPEN, QuestionType } from '../../../src/common_types/interfaces/Question';
import {
  AROUND,
  ATTESTATION,
  ONE_TO_ONE,
  ReviewType,
  SELF_ATTESTATION,
} from '../../../src/common_types/interfaces/Review';
import { ADMIN, EMPLOYEE, HR, LEADER, UserRole } from '../../../src/common_types/interfaces/User';
import { REQUIRED_FIELD_MESSAGE } from './constants';

interface ITag<T> {
  type: T;
  color: string;
  title: string;
}

/** Тип ревью -> строка */
export const reviewTypeToString: (reviewType: ReviewType) => string = (reviewType: ReviewType) => {
  switch (reviewType) {
    case AROUND: return 'Оценка 360';
    case SELF_ATTESTATION: return 'Самооценка';
    case ATTESTATION: return 'Аттестация';
    case ONE_TO_ONE: return 'ONE TO ONE';
  }
};

/** Тип вопроса -> строка */
export const questionTypeToString: (questionType: QuestionType) => string = (questionType: QuestionType) => {
  switch (questionType) {
    case CLOSED: return 'Закрытый';
    case OPEN: return 'Открытый';
  }
};

/** Тип роли -> строка */
export  const userRoleToString = (role: UserRole) => {
  switch (role) {
    case ADMIN: return 'Администриратор';
    case HR: return 'HR';
    case LEADER: return 'Руководитель';
    case EMPLOYEE: return 'Сотрудник';
    default: return '';
  }
};

export const getReviewTag: (type: ReviewType) => ITag<ReviewType> = (type: ReviewType) => {
  switch (type) {
    case ATTESTATION:
      return {
        color: '#007EE3',
        title: reviewTypeToString(type),
        type: ATTESTATION,
      };
    case SELF_ATTESTATION:
      return {
        color: '#E55741',
        title: reviewTypeToString(type),
        type: SELF_ATTESTATION,
      };
    case AROUND:
      return {
        color: '#23B7A2',
        title: reviewTypeToString(type),
        type: AROUND,
      };
    default:
      throw new Error(`Не найдено для типа "${type}"`);
  }
};

export const getRoleTag: (role: UserRole) => ITag<UserRole> = (role: UserRole) => {
  switch (role) {
    case ADMIN:
      return {
        color: '#36ff00',
        title: userRoleToString(role),
        type: ADMIN,
      };
    case HR:
      return {
        color: '#cf09f3',
        title: userRoleToString(role),
        type: HR,
      };
    case EMPLOYEE:
      return {
        color: '#ffe305',
        title: userRoleToString(role),
        type: EMPLOYEE,
      };
    case LEADER:
      return {
        color: '#000dff',
        title: userRoleToString(role),
        type: LEADER,
      };
    default:
      throw new Error(`Не найдено типа для "${role}"`);
  }
};

/** Простое обязательное поле */
export const simpleRequired: () => { message: string; required: boolean } = () => ({
  required: true,
  message: REQUIRED_FIELD_MESSAGE,
});
