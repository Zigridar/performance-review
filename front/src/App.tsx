import { ConfigProvider } from 'antd';
import 'antd/dist/antd.variable.css';
import ru from 'antd/lib/locale/ru_RU';
import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import { QueryParamProvider } from 'use-query-params';
import { MainPage } from './components/pages';
import { AuthProvider } from './auth';

export const PRIMARY_COLOR = '#0D4CD3';

export const App = () => {

  ConfigProvider.config({
    theme: {
      primaryColor: PRIMARY_COLOR,
    },
  });

  return (
    <BrowserRouter>
      <QueryParamProvider ReactRouterRoute={Route}>
        <ConfigProvider locale={ru}>
          <AuthProvider>
            <MainPage/>
          </AuthProvider>
        </ConfigProvider>
      </QueryParamProvider>
    </BrowserRouter>
  );
};
