import { useState } from 'react';

export const useUpdate = () => {

  const [, setCounter] = useState<number>(0);

  const update = () => setCounter(prev => ++prev);

  return update;
};