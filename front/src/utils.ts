/** Глубокая копия объекта */
import { IUser } from '../../src/common_types/interfaces/User';

export const deepCopy: <T extends object>(original: T) => T = <T extends object>(original: T) => {
  if (!original) return null;

  try {
    return JSON.parse(JSON.stringify(original)) as T;
  } catch (e) {
    console.error(e);
    return null;
  }
};

/** Форматирование ФИО */
export const formatFIO: (user: IUser) => string = (user: IUser) => {
  if (!user)
    return '';
  return `${user.lastName} ${user.name} ${user.middleName && user.middleName}`;
};

export const formatShortFIO = (user: IUser) => {
  if (!user)
    return '';

  return `${user.lastName} ${user.name[0]}. ${user.middleName && user.middleName[0]}.`;
};