import { QuestionsAPI } from '../api';
import { createEffectorModel } from './effector-model';

export const {
  effects: {
    create: createQuestionFx,
    edit: editQuestionFx,
    delete: deleteQuestionFx,
  },
} = createEffectorModel(QuestionsAPI);
