import { FormsAPI } from '../api';
import { createEffectorModel } from './effector-model';

export const {
  effects: {
    create: createFormFx,
    edit: editFormFx,
    delete: deleteFormFx,
  },
} = createEffectorModel(FormsAPI);

