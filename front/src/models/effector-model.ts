import { createEffect, Effect } from 'effector';
import { IWithId } from '../../../src/common_types/API_types';
import { CRUDType, doWithResponse } from '../api';

type EffectorModelType<MODEL> = {
  effects: {
    create: Effect<MODEL, MODEL>;
    edit: Effect<MODEL, MODEL>;
    delete: Effect<MODEL, MODEL>;
  }
};

export const createEffectorModel: <MODEL extends IWithId>(api: CRUDType<MODEL>) => EffectorModelType<MODEL> =
  <MODEL extends IWithId>(api: CRUDType<MODEL>) => {

    const createFx = createEffect<MODEL, MODEL>(async (model) => {
      const response = await api.create(model);
      return doWithResponse(response, {
        onResult: (result) => result,
      });
    });

    const editFx = createEffect<MODEL, MODEL>(async (model) => {
      const response = await api.update(model);
      return doWithResponse(response, {
        onResult: () => model,
      });
    });

    const deleteFx = createEffect<MODEL, MODEL>(async (model) => {
      const response = await api.delete(model);
      return doWithResponse(response, {
        onResult: () => model,
      });
    });

    return {
      effects: {
        create: createFx,
        edit: editFx,
        delete: deleteFx,
      },
    };
  };