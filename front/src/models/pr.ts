import { PrAPI } from '../api';
import { createEffectorModel } from './effector-model';

export const {
  effects: {
    create: createPRFx,
    edit: editPRFx,
    delete: deletePRFx,
  },
} = createEffectorModel(PrAPI);
