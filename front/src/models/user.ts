import { UsersAPI } from '../api';
import { createEffectorModel } from './effector-model';

export const {
  effects: {
    create: createUserFx,
    edit: editUserFx,
    delete: deleteUserFx,
  },
} = createEffectorModel(UsersAPI);
