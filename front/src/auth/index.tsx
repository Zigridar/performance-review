import { Modal, Spin } from 'antd';
import React, { createContext, useContext, useEffect, useState } from 'react';
import { AuthBody } from '../../../src/common_types/API_types';
import { IUser } from '../../../src/common_types/interfaces/User';
import { AuthAPI, doWithResponse } from '../api';
import { AuthPage } from '../components/pages';

type AuthContextType = {
  user: IUser;
  loading: boolean;
  login: (body: AuthBody) => Promise<void>;
  logout: () => Promise<void>;
};

const AuthContext = createContext<AuthContextType>(null);

/** Использование авторизации */
export const useAuth: () => AuthContextType = () => useContext(AuthContext);

const showErrorMessage = () => Modal.error({
  content: 'Ошибка',
});

/** Предоставляет работу с авторизацией */
export const AuthProvider: React.FC = ({ children }) => {

  /** Состояние загрузки */
  const [loading, setLoading] = useState<boolean>(false);

  /** Текущий пользователь */
  const [user, setUser] = useState<IUser>();

  /** Начальная автозагрузка по cookie */
  const [initialAutoLoad, setInitialAutoLoad] = useState<boolean>(false);

  useEffect(() => {
    setInitialAutoLoad(true);
    AuthAPI
      .getCurrentUser()
      .then((res) => {
        doWithResponse(res, {
          onResult: user => setUser(user),
        });
      })
      .catch(showErrorMessage)
      .finally(() => setInitialAutoLoad(false));
  }, []);

  /** Авторизация */
  const login: (credentials: AuthBody) => Promise<any> = (credentials: AuthBody) => {
    setLoading(true);

    return AuthAPI
      .login(credentials)
      .then(res => {
        doWithResponse(res, {
          onResult: user => setUser(user),
        });
      })
      .catch(showErrorMessage)
      .finally(() => setLoading(false));
  };

  /** Выход */
  const logout = () => {
    setLoading(true);
    return AuthAPI
      .logout()
      .then(() => setUser(null))
      .finally(() => setLoading(false));
  };

  return (
    <AuthContext.Provider value={{ logout, login, loading, user }}>
      {initialAutoLoad ?
        <Spin/> :
        (
          user ?
            children :
            <AuthPage loading={loading} login={login}/>
        )
      }
    </AuthContext.Provider>
  );
};